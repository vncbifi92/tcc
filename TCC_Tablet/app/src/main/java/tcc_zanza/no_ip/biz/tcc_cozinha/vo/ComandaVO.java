package tcc_zanza.no_ip.biz.tcc_cozinha.vo;

import android.support.annotation.ArrayRes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class ComandaVO implements Serializable {

    private int codigo;
    private int mesa =0;
    private String nome;
    private String hr_abertura;
    private int telefone =0;
    private boolean aberto;
    private List<ProdutoMesaVO> pedidos = new ArrayList<>();

    public ComandaVO() {
    }
    public ComandaVO(int codigo, int mesa, String nome, String hr_abertura, int telefone, boolean aberto,
                     List<ProdutoMesaVO> pedidos) {
        this.codigo = codigo;
        this.mesa = mesa;
        this.nome = nome;
        this.hr_abertura = hr_abertura;
        this.telefone = telefone;
        this.aberto = aberto;
        this.pedidos = pedidos;
    }
    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public int getMesa() {
        return mesa;
    }
    public void setMesa(int mesa) {
        this.mesa = mesa;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getHr_abertura() {
        return hr_abertura;
    }
    public void setHr_abertura(String hr_abertura) {
        this.hr_abertura = hr_abertura;
    }
    public int getTelefone() {
        return telefone;
    }
    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }
    public boolean isAberto() {
        return aberto;
    }
    public void setAberto(boolean aberto) {
        this.aberto = aberto;
    }
    public List<ProdutoMesaVO> getPedidos() {
        return pedidos;
    }
    public void setPedidos(List<ProdutoMesaVO> pedidos) {
        this.pedidos = pedidos;
    }
    @Override
    public String toString() {
        return "Comanda [codigo=" + codigo + ", mesa=" + mesa + ", nome=" + nome + ", hr_abertura=" + hr_abertura
                + ", telefone=" + telefone + ", aberto=" + aberto + ", pedidos=" + pedidos + "]";
    }

}
