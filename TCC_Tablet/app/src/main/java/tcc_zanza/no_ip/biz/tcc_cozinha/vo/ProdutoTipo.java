package tcc_zanza.no_ip.biz.tcc_cozinha.vo;

import java.io.Serializable;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class ProdutoTipo implements Serializable {

    private int codigo;
    private String descricao;
    private int tempo;

    public ProdutoTipo() {
    }

    public ProdutoTipo(int codigo, String descricao, int tempo) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.tempo = tempo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    @Override
    public String toString() {
        return "ProdutosTipo [codigo=" + codigo + ", descricao=" + descricao + ", tempo=" + tempo + "]";
    }

}
