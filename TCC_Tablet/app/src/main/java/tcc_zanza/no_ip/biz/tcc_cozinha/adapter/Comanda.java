package tcc_zanza.no_ip.biz.tcc_cozinha.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tcc_zanza.no_ip.biz.tcc_cozinha.R;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class Comanda extends BaseAdapter{

    List<ComandaVO> comandas;
    Activity act;

    public Comanda(List<ComandaVO> comandas, Activity act){
        this.comandas = comandas;
        this.act = act;
    }

    @Override
    public int getCount() {
        return comandas.size();
    }

    @Override
    public Object getItem(int position) {
        return comandas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return comandas.get(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ComandaVO comanda = (ComandaVO)getItem(position);

        LayoutInflater inflater = act.getLayoutInflater();

        View linha = inflater.inflate(R.layout.item_activity_pedido, null);

        TextView mesa_cliente = (TextView)linha.findViewById(R.id.activity_pedido_mesa_cliente);

        if(comanda.getMesa() == 0) {
            mesa_cliente.setText(comanda.getNome());
        }else{
            Integer mesa = comanda.getMesa();
            mesa_cliente.setText(mesa.toString());
        }

        TextView tempo = (TextView)linha.findViewById(R.id.activity_pedido_tempo);

        Integer tempoComanda = 0;

        for(ProdutoMesaVO produtoMesaVO : comanda.getPedidos()){
            if(produtoMesaVO.getTempo()/**produtoMesaVO.getQtd()*/ > tempoComanda) {
                tempoComanda = (produtoMesaVO.getTempo() /** produtoMesaVO.getQtd()*/);
            }

        }
        tempo.setText(tempoComanda.toString());

        if(position%2 == 0){
            linha.setBackgroundColor(Color.rgb(240, 226, 168));
        }else{
            linha.setBackgroundColor(Color.rgb(239,218,199));
        }

        return linha;
    }
}
