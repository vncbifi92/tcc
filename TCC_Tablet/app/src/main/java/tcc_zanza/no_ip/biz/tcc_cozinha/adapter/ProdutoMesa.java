package tcc_zanza.no_ip.biz.tcc_cozinha.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_cozinha.R;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.Ingrediente;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class ProdutoMesa extends BaseAdapter{

    List<ProdutoMesaVO> pedidos;
    Activity act;

    public ProdutoMesa(List<ProdutoMesaVO> pedidos, Activity act){
        this.pedidos = pedidos;
        this.act = act;
    }

    @Override
    public int getCount() {
        return pedidos.size();
    }

    @Override
    public Object getItem(int position) {
        return pedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pedidos.get(position).getCodigoM();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ProdutoMesaVO produtoMesa = (ProdutoMesaVO) getItem(position);

        LayoutInflater inflater = act.getLayoutInflater();

        View linha = inflater.inflate(R.layout.item_activity_producao, null);

        TextView activity_producao_descricao = (TextView)linha.findViewById(R.id.activity_producao_descricao);

        TextView activity_producao_preco = (TextView)linha.findViewById(R.id.activity_producao_preco);

        ListView activity_producao_lista_ingrediente = (ListView)linha.findViewById(R.id.activity_producao_lista_ingrediente);

        activity_producao_descricao.setText(produtoMesa.getDescricao());

        Double precoInt = produtoMesa.getPrecoM()/produtoMesa.getQtd();

        activity_producao_preco.setText(precoInt.toString());

        List<Ingrediente> ingredientes = new ArrayList<>();
        if(produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
            for (Ingrediente ingrediente : produtoMesa.getAdicionados()) {
                ingredientes.add(ingrediente);
            }
        }

        if(produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
            for (Ingrediente ingrediente : produtoMesa.getRemovidos()) {
                ingredientes.add(ingrediente);
            }
        }

        if((produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty())
                || produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
            IngredienteAdapter adapter = new IngredienteAdapter(ingredientes, act);
            ViewGroup.LayoutParams params = activity_producao_lista_ingrediente.getLayoutParams();
            params.height = 68*ingredientes.size();
            activity_producao_lista_ingrediente.setAdapter(adapter);
        }

        if(position%2 == 0){
            linha.setBackgroundColor(Color.rgb(240, 226, 168));
        }else{
            linha.setBackgroundColor(Color.rgb(239, 218, 199));
        }

        return linha;
    }
}
