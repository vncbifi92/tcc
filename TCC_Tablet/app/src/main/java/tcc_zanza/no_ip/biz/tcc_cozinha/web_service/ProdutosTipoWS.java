package tcc_zanza.no_ip.biz.tcc_cozinha.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;

import tcc_zanza.no_ip.biz.tcc_cozinha.dao.ProdutosTipoDAO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoTipo;

/**
 * Created by VINICIUS on 9/26/2015.
 */
public class ProdutosTipoWS extends AsyncTask<Void, Void, Void> {

    private String url;
    private int acao;
    //private ProgressDialog progress;
    private Activity activity;
    private ProdutoTipo produtoTipo = null;
    private float demora;

    public ProdutosTipoWS(float demora, Activity activity, String url, int acao){
        this.activity = activity;
        this.acao = acao;
        this.url = url;
        this.demora = demora;
    }

    public void setEntity(ProdutoTipo produtoTipo){
        this.produtoTipo = produtoTipo;
    }

    @Override
    protected Void doInBackground(Void... params) {

        ProdutosTipoDAO dao = new ProdutosTipoDAO();

        switch (acao){
            case 3:
                if(this.produtoTipo != null){
                    try {
                        dao.atualizaTempo(this.url, this.demora, this.produtoTipo);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        return null;
    }

}
