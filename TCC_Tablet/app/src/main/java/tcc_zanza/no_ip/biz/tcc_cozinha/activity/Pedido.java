package tcc_zanza.no_ip.biz.tcc_cozinha.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import tcc_zanza.no_ip.biz.tcc_cozinha.R;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.web_service.ComandaWS;
import tcc_zanza.no_ip.biz.tcc_cozinha.web_service.ProdutoMesaWS;

public class Pedido extends AppCompatActivity {

   // private static String URLComanda = "http://177.82.71.186:8080/TCC_RS/comandas/";
   // private static String URLProduto = "http://177.82.71.186:8080/TCC_RS/produtos_mesas/";
    private static String URLComanda;// = "http://10.0.137.200:8080/TCC_RS/comandas/";
    private static String URLProduto;// = "http://10.0.137.200:8080/TCC_RS/produtos_mesas/";
    private String tipo;
    private ListView listaDeComanda;
    private ComandaVO comanda;
    private String ip;
    int contador = 0;
    TimerTask mTimerTask;
    final Handler handler = new Handler();
    Timer t = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        doTimerTask();
        tipo = (String)getIntent().getSerializableExtra("tipo");
        ip = (String)getIntent().getSerializableExtra("ip");

        URLComanda = "http://"+ip+":8080/TCC_RS/comandas/";
        URLProduto = "http://"+ip+":8080/TCC_RS/produtos_mesas/";

        listaDeComanda = (ListView)findViewById(R.id.activity_pedido_lista);

        listaDeComanda.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                comanda = (ComandaVO) parent.getItemAtPosition(position);
                for (ProdutoMesaVO pedido : comanda.getPedidos()) {
                    ProdutoMesaWS ws = new ProdutoMesaWS(Pedido.this, URLProduto, 3);
                    ws.setEntity(pedido);
                    ws.execute();
                }
                Intent intent = new Intent(Pedido.this, Producao.class);

                intent.putExtra("comanda", comanda);
                intent.putExtra("url", URLProduto);
                intent.putExtra("ip", ip);
                intent.putExtra("tipo", tipo);

                startActivity(intent);
            }
        });
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        doTimerTask();
    }*/

    /*@Override
    protected void onPause() {
        super.onPause();
        doTimerTaskTempo();
    }*/

    private void alteraTempo(){
        ProdutoMesaWS ws = new ProdutoMesaWS(Pedido.this, URLProduto, 4);
        ws.execute();
    }
    private void carregaLista(){

        listaDeComanda = (ListView)findViewById(R.id.activity_pedido_lista);
        ComandaWS ws = new ComandaWS(tipo, listaDeComanda, this, URLComanda, 2);

        ws.execute();
    }


    /*public void doTimerTaskTempo(){
        mTimerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        alteraTempo();
                    }
                });
            }};
        t.schedule(mTimerTask, 1000*60, 1000*60);
    }*/


    public void doTimerTask(){
        mTimerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        if (contador == 6) {
                            contador = 0;
                            alteraTempo();
                        }
                        contador++;
                        carregaLista();

                    }
                });
            }};
        t.schedule(mTimerTask, 500, 10000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){

            case R.id.menu_aba_embalagem:

                intent = new Intent(this, Embalagem.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_aba_pedido:

                intent = new Intent(this, Pedido.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_aba_producao:

                intent = new Intent(this, Producao.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
