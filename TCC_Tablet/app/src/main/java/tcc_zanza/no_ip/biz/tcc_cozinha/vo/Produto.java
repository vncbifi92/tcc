package tcc_zanza.no_ip.biz.tcc_cozinha.vo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class Produto implements Serializable {

    private int codigo;
    private String descricao;
    private double preco;
    private ProdutoTipo tipo;
    private ArrayList<Ingrediente> ingredientes;

    public Produto() {
    }

    public Produto(int codigo, String descricao, double preco,
                   ProdutoTipo tipo, ArrayList<Ingrediente> ingredientes) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.preco = preco;
        this.tipo = tipo;
        this.ingredientes = ingredientes;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public ProdutoTipo getTipo() {
        return tipo;
    }

    public void setTipo(ProdutoTipo tipo) {
        this.tipo = tipo;
    }

    public ArrayList<Ingrediente> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(ArrayList<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }

    @Override
    public String toString() {
        return "Produto [codigo=" + codigo + ", descricao=" + descricao + ", preco=" + preco + ", tipo=" + tipo
                + ", ingredientes=" + ingredientes + "]";
    }

}
