package tcc_zanza.no_ip.biz.tcc_cozinha.dao;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoTipo;

/**
 * Created by VINICIUS on 9/25/2015.
 */
public class ProdutosTipoDAO {

    public void atualizaTempo (String url, float tempoAtual, ProdutoTipo produtosTipo) throws JSONException, IOException {

        HttpClient httpClient = new DefaultHttpClient();

        HttpPut putTipo = new HttpPut(url);

        produtosTipo.setTempo((int) (((produtosTipo.getTempo() * 2) + tempoAtual) / 3));

        JSONObject json = new JSONObject();
        json.put("codigo", produtosTipo.getCodigo());
        json.put("descricao", produtosTipo.getDescricao());
        if(produtosTipo.getTempo() < 1){
            json.put("tempo", 1);
        }else{
            json.put("tempo", produtosTipo.getTempo());
        }

        JSONObject jTipo = new JSONObject();

        jTipo.put("produtosTipo", json);

        StringEntity se = new StringEntity(jTipo.toString());
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        putTipo.setEntity(se);

        httpClient.execute(putTipo);

    }

}
