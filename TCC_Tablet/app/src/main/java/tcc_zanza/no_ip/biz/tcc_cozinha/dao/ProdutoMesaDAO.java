package tcc_zanza.no_ip.biz.tcc_cozinha.dao;

import android.util.Log;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 9/20/2015.
 */
public class ProdutoMesaDAO {

    public void atualizaProdutoMesa(String url, ProdutoMesaVO produtoMesa) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();

        HttpPut putComanda = new HttpPut(url+"situacao/"+produtoMesa.getCodigoM());

        httpClient.execute(putComanda);
    }

    public void atualizaTempoProdutoMesa(String url) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();

        HttpPut putComanda = new HttpPut(url+"tempo/");

        httpClient.execute(putComanda);
    }

}
