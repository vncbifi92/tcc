package tcc_zanza.no_ip.biz.tcc_cozinha.vo;

import java.io.Serializable;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class Ingrediente implements Serializable{

    private int codigo;
    private String descricao;
    private String unidade;
    private double preco;

    public Ingrediente(){

    }

    public Ingrediente(int codigo, String descricao, String unidade,
                       double preco) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.unidade = unidade;
        this.preco = preco;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "Ingrediente [descricao=" + descricao + ", unidade=" + unidade
                + ", preco=" + preco +"]";
    }

}
