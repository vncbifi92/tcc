package tcc_zanza.no_ip.biz.tcc_cozinha.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_cozinha.R;
import tcc_zanza.no_ip.biz.tcc_cozinha.adapter.ProdutoMesa;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.Ingrediente;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoTipo;
import tcc_zanza.no_ip.biz.tcc_cozinha.web_service.ComandaWS;
import tcc_zanza.no_ip.biz.tcc_cozinha.web_service.ProdutoMesaWS;
import tcc_zanza.no_ip.biz.tcc_cozinha.web_service.ProdutosTipoWS;

public class Embalagem extends AppCompatActivity {
    private ComandaVO comanda = null;
    private ListView listaPrdidos;
    private TextView mesa_cliente;
    private Button finalizado;
    private String URL;
   // private String URLProdutoTipo = "http://177.82.71.186:8080/TCC_RS/produtos_tipo/";
    private String URLProdutoTipo;// = "http://10.0.137.200:8080/TCC_RS/produtos_tipo/";
    private String tipo;
    private long inicio;
    private Date fimMili = new Date();
    private long fim;
    private float demora;
    private int totalPedidos=0;
    private ProdutoTipo produtoTipo;
    private String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_embalagem);

        listaPrdidos = (ListView)findViewById(R.id.activity_embalagem_lista);
        mesa_cliente = (TextView)findViewById(R.id.activity_embalagem_cliente_mesa);
        finalizado = (Button)findViewById(R.id.activity_embalagem_finalizado);
        tipo = (String)getIntent().getSerializableExtra("tipo");
        comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");
        URL = (String)getIntent().getSerializableExtra("url");
        inicio = (long)getIntent().getSerializableExtra("inicio");
        ip = (String)getIntent().getSerializableExtra("ip");

        URLProdutoTipo = "http://"+ip+":8080/TCC_RS/produtos_tipo/";

        finalizado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fim = fimMili.getTime();
                demora = ((fim - inicio)/1000)/60;
                for(ProdutoMesaVO produto : comanda.getPedidos()) {
                    ProdutoMesaWS ws = new ProdutoMesaWS(Embalagem.this, URL, 3);
                    ws.setEntity(produto);
                    ws.execute();
                    produtoTipo = produto.getTipo();
                    totalPedidos = totalPedidos + produto.getQtd();
                }

                demora = demora/totalPedidos;

                ProdutosTipoWS ws = new ProdutosTipoWS(demora, Embalagem.this, URLProdutoTipo+produtoTipo.getDescricao().replace(" ", "%20"), 3);

                ws.setEntity(produtoTipo);

                ws.execute();

                finish();

                /*Intent intent =  new Intent(Embalagem.this, Pedido.class);
                intent.putExtra("tipo", tipo);
                intent.putExtra("ip", ip);
                startActivity(intent);*/

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){

            case R.id.menu_aba_embalagem:

                intent = new Intent(this, Embalagem.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_aba_pedido:

                intent = new Intent(this, Pedido.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_aba_producao:

                intent = new Intent(this, Producao.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private  void carregaLista(){

        List<ProdutoMesaVO> produtos = new ArrayList<>();
        if(comanda != null) {
            for(ProdutoMesaVO produtoMesa : comanda.getPedidos()) {
                List<Ingrediente> ingredientes = new ArrayList<>();
                if (produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
                    for (Ingrediente ingrediente : produtoMesa.getAdicionados()) {
                        ingredientes.add(ingrediente);
                    }
                }

                if (produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
                    for (Ingrediente ingrediente : produtoMesa.getRemovidos()) {
                        ingredientes.add(ingrediente);
                    }
                }
                produtoMesa.setPreco(produtoMesa.getPrecoM()/produtoMesa.getQtd());
                for(int i = 0 ; i<produtoMesa.getQtd() ; i++){
                    produtos.add(produtoMesa);
                }
            }

            ProdutoMesa adapter = new ProdutoMesa(produtos, this);

            listaPrdidos.setAdapter(adapter);

            if(comanda.getMesa() == 0) {
                if(comanda.getTelefone() == 0) {
                mesa_cliente.setText("Cliente: "+comanda.getNome());
            }else{
                Integer telefone = comanda.getTelefone();
                mesa_cliente.setText("Cliente/Fone:  "+comanda.getNome() + " / "+telefone.toString());
            }
            }else{
                Integer mesa = comanda.getMesa();
                mesa_cliente.setText("Mesa: " + mesa.toString());
            }

        }

    }

}
