package tcc_zanza.no_ip.biz.tcc_cozinha.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_cozinha.adapter.Comanda;
import tcc_zanza.no_ip.biz.tcc_cozinha.dao.ComandaDAO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class ComandaWS extends AsyncTask<Void, Void, List<ComandaVO>>{

    private String url;
    private int acao;
    //private ProgressDialog progress;
    private Activity activity;
    private ListView listaDeComanda;
    private ComandaVO comanda = null;
    private String tipo;

    public ComandaWS(String tipo, ListView listaDeComanda, Activity activity, String url, int acao){
        this.activity = activity;
        this.listaDeComanda = listaDeComanda;
        this.acao = acao;
        this.url = url;
        this.tipo = tipo;
    }

    public void setEntity(ComandaVO comanda){
        this.comanda = comanda;
    }

    /*@Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(activity, "Aguarde..", "Carregando dados do servidor.");
    }*/

    @Override
    protected List<ComandaVO> doInBackground(Void... params) {

        List<ComandaVO> comandas = new ArrayList<>();

        ComandaDAO dao = new ComandaDAO();

        switch (acao){
            case 1:
                try {
                    comandas = dao.buscaTodasComandas(url);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;

            case 2:
                try {
                    dao.setTipo(tipo);

                    comandas = dao.buscaTodasComandas(url);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;
            /*case 3:
                if(this.comanda != null){
                    try {
                        dao.atualizaComanda(this.url, this.comanda);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;*/

        }

        return comandas;
    }

    @Override
    protected void onPostExecute(List<ComandaVO> comandas) {
        //progress.dismiss();

        if(comandas.size() > 0){
            Comanda adapter = new Comanda(comandas, activity);

            listaDeComanda.setAdapter(adapter);
        }
    }
}
