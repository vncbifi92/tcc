package tcc_zanza.no_ip.biz.tcc_cozinha.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ListView;

import java.io.IOException;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_cozinha.dao.ProdutoMesaDAO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 9/21/2015.
 */
public class ProdutoMesaWS extends AsyncTask<Void, Void, Void> {

    private String url;
    private int acao;
    //private ProgressDialog progress;
    private Activity activity;
    private ProdutoMesaVO produtoMesa = null;

    public ProdutoMesaWS(Activity activity, String url, int acao){
        this.activity = activity;
        this.acao = acao;
        this.url = url;
    }

    public void setEntity(ProdutoMesaVO produtoMesa){
        this.produtoMesa = produtoMesa;
    }

    /*@Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(activity, "Aguarde..", "Carregando dados do servidor.");
    }*/
    @Override
    protected Void doInBackground(Void... params) {

        ProdutoMesaDAO dao = new ProdutoMesaDAO();

        switch (acao){
            case 3:
                if(this.produtoMesa != null){
                    try {
                        dao.atualizaProdutoMesa(this.url, this.produtoMesa);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 4:
                try{
                    dao.atualizaTempoProdutoMesa(this.url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return null;
    }
    /*@Override
    protected void onPostExecute(Void v) {
        progress.dismiss();
    }*/

}
