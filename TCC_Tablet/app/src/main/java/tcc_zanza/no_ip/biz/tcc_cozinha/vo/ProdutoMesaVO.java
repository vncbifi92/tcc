package tcc_zanza.no_ip.biz.tcc_cozinha.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class ProdutoMesaVO extends Produto implements Serializable {

    private int codigoM;
    private int qtd;
    private double precoM;
    private String situacao;
    private String horaEntrada;
    private String horaTermino;
    private boolean ativo;
    private List<Ingrediente> removidos;
    private List<Ingrediente> adicionados;
    private int tempo;

    public ProdutoMesaVO() {
        super();
    }

    public ProdutoMesaVO(int codigo, int qtd,/* int mesa, String nome,*/ double preco, String situacao, String horaEntrada, String horaTermino,
                         boolean ativo, List<Ingrediente> removidos, List<Ingrediente> adicionados, int tempo, Produto produto) {
        super(produto.getCodigo(), produto.getDescricao(), produto.getPreco(), produto.getTipo(), produto.getIngredientes());
        this.codigoM = codigo;
        this.qtd = qtd;
        //this.mesa = mesa;
        //this.nome = nome;
        this.precoM = preco;
        this.situacao = situacao;
        this.horaEntrada = horaEntrada;
        this.horaTermino = horaTermino;
        this.ativo = ativo;
        this.removidos = removidos;
        this.adicionados = adicionados;
        this.tempo = tempo;
    }

    public int getTempo(){
        return this.tempo;
    }
    public void setTempo(int tempo){
        this.tempo = tempo;
    }
    public int getCodigoM() {
        return codigoM;
    }
    public void setCodigoM(int codigo) {
        this.codigoM = codigo;
    }
    public int getQtd() {
        return qtd;
    }
    public void setQtd (int qtd){
        this.precoM = super.getPreco()*qtd;

        if (this.adicionados != null && !this.adicionados.isEmpty()){
            for(Ingrediente ingrediente : this.adicionados){
                this.precoM = this.precoM+ingrediente.getPreco();
            }
        }

        if (this.removidos != null && !this.removidos.isEmpty()){
            for(Ingrediente ingrediente : this.removidos){
                this.precoM = this.precoM+ingrediente.getPreco();
            }
        }
        this.qtd = qtd;
    }
    public double getPrecoM() {
        return precoM;
    }
    public void setPrecoM(double preco) {
        this.precoM = preco;
    }
    public String getSituacao() {
        return situacao;
    }
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
    public String getHoraEntrada() {
        return horaEntrada;
    }
    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }
    public String getHoraTermino() {
        return horaTermino;
    }
    public void setHoraTermino(String horaTermino) {
        this.horaTermino = horaTermino;
    }
    public boolean isAtivo() {
        return ativo;
    }
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    public List<Ingrediente> getRemovidos() {
        return removidos;
    }
    public void setRemovidos(List<Ingrediente> removidos) {
        this.removidos = removidos;
    }
    public List<Ingrediente> getAdicionados() {
        return adicionados;
    }
    public void setAdicionados(List<Ingrediente> adicionados) {
        this.adicionados = adicionados;
    }

    public void setProduto(Produto produto){
        super.setCodigo(produto.getCodigo());
        super.setDescricao(produto.getDescricao());
        super.setIngredientes(produto.getIngredientes());
        super.setPreco(produto.getPreco());
        super.setTipo(produto.getTipo());
    }

    @Override
    public String toString() {
        return "ProdutoMesa [codigoM=" + codigoM + ", precoM=" + precoM + ", situacao=" + situacao + ", horaEntrada="
                + horaEntrada + ", horaTermino=" + horaTermino + ", ativo=" + ativo + ", removidos=" + removidos
                + ", adicionados=" + adicionados + "]";
    }

}
