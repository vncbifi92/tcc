package tcc_zanza.no_ip.biz.tcc_cozinha.dao;

import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_cozinha.adapter.ProdutoMesa;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.Ingrediente;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.Produto;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoTipo;

/**
 * Created by VINICIUS on 9/19/2015.
 */
public class ComandaDAO {

    private String tipo;

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

    private ProdutoMesaVO produtoFromJson(JSONObject jPedidos) throws JSONException {

        ProdutoTipo tipo;
        List<Ingrediente> adicionados = new ArrayList<>();
        List<Ingrediente> removidos = new ArrayList<>();
        ArrayList<Ingrediente> ingredientes = new ArrayList<>();
        Gson gson = new Gson();

        JSONObject jTipo = jPedidos.getJSONObject("tipo");

        tipo = gson.fromJson(jTipo.toString(), ProdutoTipo.class);

        if(jPedidos.has("adicionados")) {
            try {
                JSONArray jAdicionados = jPedidos.getJSONArray("adicionados");
                Ingrediente adicionado = new Ingrediente();
                for(int i = 0 ; i<jAdicionados.length() ; i++){
                    JSONObject jAdicionado = jAdicionados.getJSONObject(i);

                    adicionado = gson.fromJson(jAdicionado.toString(), Ingrediente.class);

                    adicionados.add(adicionado);
                }

            } catch (JSONException e2) {
                Ingrediente adicionado = new Ingrediente();
                JSONObject jAdicionados = jPedidos.getJSONObject("adicionados");
                adicionado = gson.fromJson(jAdicionados.toString(), Ingrediente.class);

                adicionados.add(adicionado);
            }
        }

        if(jPedidos.has("removidos")) {
            try {
                JSONArray jRemovidos = jPedidos.getJSONArray("removidos");
                Ingrediente removido = new Ingrediente();
                for(int i = 0 ; i<jRemovidos.length() ; i++){
                    JSONObject jRemovido = jRemovidos.getJSONObject(i);

                    removido = gson.fromJson(jRemovido.toString(), Ingrediente.class);

                    removidos.add(removido);
                }

            } catch (JSONException e2) {
                Ingrediente removido = new Ingrediente();
                JSONObject jRemovidos = jPedidos.getJSONObject("removidos");
                removido = gson.fromJson(jRemovidos.toString(), Ingrediente.class);

                removidos.add(removido);
            }
        }

        try {
            JSONArray jIngredientes = jPedidos.getJSONArray("ingredientes");
            Ingrediente ingrediente = new Ingrediente();
            for(int i = 0 ; i<jIngredientes.length() ; i++){
                JSONObject jIngredient = jIngredientes.getJSONObject(i);

                ingrediente = gson.fromJson(jIngredient.toString(), Ingrediente.class);

                ingredientes.add(ingrediente);
            }

        } catch (JSONException e2) {
            JSONObject jIngredient = jPedidos.getJSONObject("ingredientes");
            Ingrediente ingrediente = new Ingrediente();
            ingrediente = gson.fromJson(jIngredient.toString(), Ingrediente.class);

            ingredientes.add(ingrediente);
        }

        ProdutoMesaVO pedido = new ProdutoMesaVO(
                jPedidos.getInt("codigoM")
                , jPedidos.getInt("qtd")
                , new Double(jPedidos.getDouble("precoM")).doubleValue()
                , jPedidos.getString("situacao")
                , jPedidos.getString("horaEntrada")
                , null
                , jPedidos.getBoolean("ativo")
                , removidos
                , adicionados
                , jPedidos.getInt("tempo")
                , new Produto(
                jPedidos.getInt("codigo")
                , jPedidos.getString("descricao")
                , new Double(jPedidos.getDouble("preco")).doubleValue()
                , tipo
                , ingredientes
        )
        );

        return pedido;

    }

    private ComandaVO buscaComanda(String url) throws IOException, JSONException {

        List<ProdutoMesaVO> pedidos = new ArrayList<>();

        ComandaVO comandas;

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getComanda = new HttpGet(url);

        HttpResponse response = httpClient.execute(getComanda);

        HttpEntity entity = response.getEntity();

        JSONObject jComandas = new JSONObject(EntityUtils.toString(entity)).getJSONObject("comanda");

        try {
            JSONArray jPedidos = jComandas.getJSONArray("pedidos");
            for(int i = 0 ; i<jPedidos.length() ; i++){

                JSONObject jPedido = jPedidos.getJSONObject(i);
                pedidos.add(produtoFromJson(jPedido));
            }
        }catch(JSONException e1){
            JSONObject jPedido = jComandas.getJSONObject("pedidos");
            pedidos.add(produtoFromJson(jPedido));
        }
        if(jComandas.has("nome")){
            comandas = new ComandaVO(
                    jComandas.getInt("codigo")
                    , jComandas.getInt("mesa")
                    , jComandas.getString("nome")
                    , jComandas.getString("hr_abertura")
                    , jComandas.getInt("telefone")
                    , jComandas.getBoolean("aberto")
                    , pedidos
            );
        }else{
            comandas = new ComandaVO(
                    jComandas.getInt("codigo")
                    , jComandas.getInt("mesa")
                    , null
                    , jComandas.getString("hr_abertura")
                    , jComandas.getInt("telefone")
                    , jComandas.getBoolean("aberto")
                    , pedidos
            );
        }

        return comandas;
    }

    public List<ComandaVO> buscaTodasComandas(String url) throws IOException, JSONException {


        boolean nova;
        List<ProdutoMesaVO> produtos;
        ComandaVO comanda;
        List<ComandaVO> comandas = new ArrayList<ComandaVO>();

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getComanda = new HttpGet(url);
        HttpResponse response = httpClient.execute(getComanda);
        HttpEntity entity = response.getEntity();

        JSONObject jsonn  = new JSONObject(EntityUtils.toString(entity));

        JSONObject json = jsonn.getJSONObject("comandas");

        String title;
        JSONArray links = null;
        try{
            links = json.getJSONArray("link");
        } catch (JSONException e){

            JSONObject linkk = json.getJSONObject("link");

            title = linkk.getString("@title");

            comanda = this.buscaComanda(url+title.replace(" ", "%20"));

            if(comanda.isAberto()) {
                nova = false;
                produtos = new ArrayList<>();
                for(ProdutoMesaVO produto : comanda.getPedidos()) {

                    if (produto.isAtivo() && produto.getSituacao().equals("Aguardando Producao") && produto.getTipo().getDescricao().equals(tipo)) {
                        nova = true;
                        produtos.add(produto);
                    }
                }

                comanda.setPedidos(produtos);

                if (nova && !comanda.getPedidos().isEmpty()) {
                    comandas.add(comanda);
                }
            }
            return comandas;
        }

        for(int i = 0 ; i<links.length() ; i++){
            JSONObject link = links.getJSONObject(i);

            title = link.getString("@title");

            comanda = this.buscaComanda(url + title.replace(" ", "%20"));
            if(comanda.isAberto()) {
                nova = false;
                produtos = new ArrayList<>();
                for(ProdutoMesaVO produto : comanda.getPedidos()) {
                    if (produto.isAtivo() && produto.getSituacao().equals("Aguardando Producao") && produto.getTipo().getDescricao().equals(tipo)) {
                        nova = true;
                        produtos.add(produto);
                    }
                }

                comanda.setPedidos(produtos);

                if (nova && !comanda.getPedidos().isEmpty()) {
                    comandas.add(comanda);
                }
            }
        }
        return comandas;

    }

}
