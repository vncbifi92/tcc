package tcc_zanza.no_ip.biz.tcc_cozinha.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import tcc_zanza.no_ip.biz.tcc_cozinha.R;
import tcc_zanza.no_ip.biz.tcc_cozinha.adapter.ProdutoMesa;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.Ingrediente;
import tcc_zanza.no_ip.biz.tcc_cozinha.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_cozinha.web_service.ProdutoMesaWS;

public class Producao extends AppCompatActivity {

    private ComandaVO comanda = null;
    private ListView listaPrdidos;
    private TextView mesa_cliente;
    private Button finalizado;
    private String URL;
    private TextView timer;
    private int nCounter = 0;
    private String tipo;
    private long inicio;
    private Date inicioMili = new Date();
    private String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_producao);
        timer = (TextView)findViewById(R.id.activity_producao_timer);
        listaPrdidos = (ListView)findViewById(R.id.activity_producao_lista);
        mesa_cliente = (TextView)findViewById(R.id.activity_producao_cliente_mesa);
        finalizado = (Button)findViewById(R.id.activity_producao_finalizado);
        ip = (String)getIntent().getSerializableExtra("ip");

        comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");
        URL = (String)getIntent().getSerializableExtra("url");
        tipo = (String)getIntent().getSerializableExtra("tipo");

        for(ProdutoMesaVO produto : comanda.getPedidos()){
            if(produto.getTempo() > nCounter) {
                nCounter = nCounter + produto.getTempo();
            }
        }
        inicio = inicioMili.getTime();
        doTimerTask();

        finalizado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(ProdutoMesaVO pedido : comanda.getPedidos()) {
                    ProdutoMesaWS ws = new ProdutoMesaWS(Producao.this, URL, 3);
                    ws.setEntity(pedido);
                    ws.execute();
                }

                Intent intent =  new Intent(Producao.this, Embalagem.class);

                intent.putExtra("comanda", comanda);
                intent.putExtra("tipo", tipo);
                intent.putExtra("url", URL);
                intent.putExtra("inicio", inicio);
                intent.putExtra("ip", ip);

                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){

            case R.id.menu_aba_embalagem:

                intent = new Intent(this, Embalagem.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_aba_pedido:

                intent = new Intent(this, Pedido.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            case R.id.menu_aba_producao:

                intent = new Intent(this, Producao.class);
                intent.putExtra("tipo", tipo);
                startActivity(intent);
                finish();
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    TimerTask mTimerTask;
    final Handler handler = new Handler();
    Timer t = new Timer();

    public void doTimerTask(){
        mTimerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        timer.setText("Tempo: " + nCounter + " minutos.");
                        if(nCounter >0) {
                            nCounter--;
                        }
                    }
                });
            }};

        t.schedule(mTimerTask, 500, 1000*60);
    }

    private  void carregaLista(){
        List<ProdutoMesaVO> produtos = new ArrayList<>();
        if(comanda != null) {
            for(ProdutoMesaVO produtoMesa : comanda.getPedidos()) {
                List<Ingrediente> ingredientes = new ArrayList<>();
                if (produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
                    for (Ingrediente ingrediente : produtoMesa.getAdicionados()) {
                        ingrediente.setDescricao("Com " + ingrediente.getDescricao().replace(" - Especial", ""));
                        ingredientes.add(ingrediente);
                    }
                }

                if (produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
                    for (Ingrediente ingrediente : produtoMesa.getRemovidos()) {
                        ingrediente.setDescricao("Sem " + ingrediente.getDescricao().replace(" - Especial", ""));
                        ingredientes.add(ingrediente);
                    }
                }
                produtoMesa.setPreco(produtoMesa.getPrecoM()/produtoMesa.getQtd());
                for(int i = 0 ; i<produtoMesa.getQtd() ; i++){
                    produtos.add(produtoMesa);
                }
            }

            ProdutoMesa adapter = new ProdutoMesa(produtos, this);

            listaPrdidos.setAdapter(adapter);

            if(comanda.getMesa() == 0) {
                    mesa_cliente.setText("Cliente: "+comanda.getNome());
            }else{
                Integer mesa = comanda.getMesa();
                mesa_cliente.setText("Mesa: " + mesa.toString());
            }

        }

    }
}
