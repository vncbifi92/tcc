package tcc_zanza.no_ip.biz.tcc_cozinha.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import tcc_zanza.no_ip.biz.tcc_cozinha.R;

public class InicioActivity extends AppCompatActivity {

    private Button pizza_especial;
    private Button pizza;
    private Button pastel_especial;
    private Button pastel;
    private EditText ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        pizza_especial = (Button) findViewById(R.id.activity_inicio_pizza_especial);
        pizza = (Button) findViewById(R.id.activity_inicio_pizza);
        pastel_especial = (Button) findViewById(R.id.activity_inicio_pastel_especial);
        pastel = (Button) findViewById(R.id.activity_inicio_pastel);
        ip = (EditText) findViewById(R.id.ip_servidor);
       pizza_especial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ip.getText().equals("")){
                    Toast.makeText(InicioActivity.this, "IP não informado", Toast.LENGTH_LONG).show();
                }else {
                    Intent intent = new Intent(InicioActivity.this, Pedido.class);
                    intent.putExtra("tipo", "Pizza - Especial");
                    intent.putExtra("ip", ip.getText().toString());
                    startActivity(intent);
                }
            }
        });

        pizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ip.getText().equals("")){
                    Toast.makeText(InicioActivity.this, "IP não informado", Toast.LENGTH_LONG).show();
                }else {
                    Intent intent = new Intent(InicioActivity.this, Pedido.class);
                    intent.putExtra("tipo", "Pizza");
                    intent.putExtra("ip", ip.getText().toString());
                    startActivity(intent);
                }
            }
        });

        pastel_especial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ip.getText().equals("")){
                    Toast.makeText(InicioActivity.this, "IP não informado", Toast.LENGTH_LONG).show();
                }else {
                    Intent intent = new Intent(InicioActivity.this, Pedido.class);
                    intent.putExtra("tipo", "Pastel - Especial");
                    intent.putExtra("ip", ip.getText().toString());
                    startActivity(intent);
                }
            }
        });

        pastel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ip.getText().equals("")){
                    Toast.makeText(InicioActivity.this, "IP não informado", Toast.LENGTH_LONG).show();
                }else {
                    Intent intent = new Intent(InicioActivity.this, Pedido.class);
                    intent.putExtra("tipo", "Pastel");
                    intent.putExtra("ip", ip.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }



}
