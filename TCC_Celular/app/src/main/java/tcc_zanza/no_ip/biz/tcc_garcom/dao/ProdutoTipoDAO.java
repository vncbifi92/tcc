package tcc_zanza.no_ip.biz.tcc_garcom.dao;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by VINICIUS on 10/25/2015.
 */
public class ProdutoTipoDAO {

    public int getTempo (String url, String tipo) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getproduto = new HttpGet(url+"tempo/"+tipo);

        HttpResponse response = httpClient.execute(getproduto);

        HttpEntity entity = response.getEntity();

        int tempo = Integer.parseInt(EntityUtils.toString(entity));

        return tempo;

    }

}
