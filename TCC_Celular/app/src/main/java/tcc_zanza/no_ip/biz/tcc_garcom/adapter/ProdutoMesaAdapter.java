package tcc_zanza.no_ip.biz.tcc_garcom.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 10/12/2015.
 */
public class ProdutoMesaAdapter extends BaseAdapter {

    List<ProdutoMesaVO> pedidos;
    Activity act;
    ProdutoMesaVO produtoMesa;
    public ProdutoMesaAdapter(List<ProdutoMesaVO> pedidos, Activity act){
        this.pedidos = pedidos;
        this.act = act;
    }

    @Override
    public int getCount() {
        return pedidos.size();
    }

    @Override
    public Object getItem(int position) {
        return pedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pedidos.get(position).getCodigoM();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        produtoMesa = (ProdutoMesaVO) getItem(position);

        LayoutInflater inflater = act.getLayoutInflater();

        View linha = inflater.inflate(R.layout.item_activity_pedidos_da_mesa, null);

        TextView activity_pedidos_da_mesa_descricao = (TextView)linha.findViewById(R.id.activity_pedidos_da_mesa_descricao);

        TextView activity_pedidos_da_mesa_quantidade = (TextView)linha.findViewById(R.id.activity_pedidos_da_mesa_quantidade);

        //ListView activity_pedidos_da_mesa_ingrediente = (ListView)linha.findViewById(R.id.activity_pedidos_da_mesa_ingrediente);

        TextView opcionais = (TextView)linha.findViewById(R.id.opcionais);


        activity_pedidos_da_mesa_descricao.setText(produtoMesa.getDescricao());

        Integer quantidadeInt = produtoMesa.getQtd();

        activity_pedidos_da_mesa_quantidade.setText(quantidadeInt.toString());

        List<IngredienteVO> ingredientes = new ArrayList<>();
        if(produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
            for (IngredienteVO ingrediente : produtoMesa.getAdicionados()) {
                ingredientes.add(ingrediente);
            }
        }

        if(produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
            for (IngredienteVO ingrediente : produtoMesa.getRemovidos()) {
                ingredientes.add(ingrediente);
            }
        }

        if((produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty())
                || produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
            String opcional = new String();
            for(IngredienteVO ingrediente : produtoMesa.getRemovidos()){
                opcional = opcional + "\n"+ingrediente.getDescricao();
            }
            for(IngredienteVO ingrediente : produtoMesa.getAdicionados()){
                opcional = opcional + "\n"+ingrediente.getDescricao();
            }

            opcionais.setText(opcional.substring(1));

            /*IngredienteAdapter adapter = new IngredienteAdapter(ingredientes, act);
            ViewGroup.LayoutParams params = activity_pedidos_da_mesa_ingrediente.getLayoutParams();
            params.height = 100;
            activity_pedidos_da_mesa_ingrediente.setAdapter(adapter);*/
        }

        if(position%2 == 0){
            linha.setBackgroundColor(Color.rgb(240,226,168));
        }else{
            linha.setBackgroundColor(Color.rgb(239,218,199));
        }

        /*linha.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(produtoMesa.getQtd() > 0){
                    produtoMesa.setQtd(produtoMesa.getQtd()-1);
                }else {
                    pedidos.remove(produtoMesa);
                }
                return false;
            }
        });*/
        return linha;
    }

}