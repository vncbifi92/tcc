package tcc_zanza.no_ip.biz.tcc_garcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import tcc_zanza.no_ip.biz.tcc_garcom.R;

public class IPActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ip);

        Button botao_ip = (Button)findViewById(R.id.botao_ip);

        final EditText sv_ip = (EditText)findViewById(R.id.sv_ip);

        botao_ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(IPActivity.this, IndexActivity.class);

                intent.putExtra("ip", sv_ip.getText().toString());
                startActivity(intent);
            }
        });

    }

}
