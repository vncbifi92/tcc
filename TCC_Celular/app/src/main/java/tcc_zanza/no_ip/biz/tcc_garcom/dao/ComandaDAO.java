package tcc_zanza.no_ip.biz.tcc_garcom.dao;

import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.vo.*;

/**
 * Created by VINICIUS on 10/10/2015.
 */
public class ComandaDAO {

    private JSONObject jsonFromProdutoMesa(ProdutoMesaVO produtoMesa) throws JSONException {

        JSONObject jTipo = new JSONObject();
        jTipo.put("codigo", produtoMesa.getTipo().getCodigo());
        jTipo.put("descricao", produtoMesa.getTipo().getDescricao());
        jTipo.put("tempo", produtoMesa.getTipo().getTempo());


        JSONObject jProdutoMesa = new JSONObject();
        jProdutoMesa.put("codigo", produtoMesa.getCodigo());
        jProdutoMesa.put("descricao", produtoMesa.getDescricao());
        jProdutoMesa.put("preco", produtoMesa.getPreco());
        jProdutoMesa.put("tipo", jTipo);

        if(produtoMesa.getIngredientes() != null && !produtoMesa.getIngredientes().isEmpty()){
            if(produtoMesa.getIngredientes().size() > 1) {
                JSONArray jIngredienteProd = new JSONArray();
                for (IngredienteVO ingrediente : produtoMesa.getIngredientes()) {
                    JSONObject jIngrediente = new JSONObject();
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                    jIngredienteProd.put(jIngrediente);
                }
                jProdutoMesa.put("ingredientes", jIngredienteProd);
            }else{
                JSONObject jIngrediente = new JSONObject();
                for (IngredienteVO ingrediente : produtoMesa.getIngredientes()) {
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                }
                jProdutoMesa.put("ingredientes", jIngrediente);
            }
        }

        jProdutoMesa.put("codigoM", produtoMesa.getCodigoM());
        jProdutoMesa.put("qtd", produtoMesa.getQtd());
        jProdutoMesa.put("precoM", produtoMesa.getPrecoM());
        jProdutoMesa.put("situacao", produtoMesa.getSituacao());

        jProdutoMesa.put("ativo", produtoMesa.isAtivo());

        if(produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
            if (produtoMesa.getRemovidos().size() > 1) {
                JSONArray jRemovidos = new JSONArray();
                for (IngredienteVO ingrediente : produtoMesa.getRemovidos()) {
                    JSONObject jIngrediente = new JSONObject();
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                    jRemovidos.put(jIngrediente);
                }
                jProdutoMesa.put("removidos", jRemovidos);
            } else {
                JSONObject jIngrediente = new JSONObject();
                for (IngredienteVO ingrediente : produtoMesa.getRemovidos()) {
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                }
                jProdutoMesa.put("removidos", jIngrediente);
            }
        }

        if(produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
            if (produtoMesa.getAdicionados().size() > 1) {
                JSONArray jAdicionados = new JSONArray();
                for (IngredienteVO ingrediente : produtoMesa.getAdicionados()) {
                    JSONObject jIngrediente = new JSONObject();
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                    jAdicionados.put(jIngrediente);
                }
                jProdutoMesa.put("adicionados", jAdicionados);
            } else {
                JSONObject jIngrediente = new JSONObject();
                for (IngredienteVO ingrediente : produtoMesa.getAdicionados()) {
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                }
                jProdutoMesa.put("adicionados", jIngrediente);
            }
        }

        //JSONObject jProdutoMesaF = new JSONObject();
        //jProdutoMesaF.put("produtoMesa", jProdutoMesa);

        return jProdutoMesa;

    }

    private ProdutoMesaVO produtoFromJson(JSONObject jPedidos) throws JSONException {

        ProdutoTipoVO tipo;
        List<IngredienteVO> adicionados = new ArrayList<>();
        List<IngredienteVO> removidos = new ArrayList<>();
        ArrayList<IngredienteVO> IngredienteVOs = new ArrayList<>();
        Gson gson = new Gson();

        JSONObject jTipo = jPedidos.getJSONObject("tipo");

        tipo = gson.fromJson(jTipo.toString(), ProdutoTipoVO.class);

        if(jPedidos.has("adicionados")) {
            try {
                JSONArray jAdicionados = jPedidos.getJSONArray("adicionados");
                IngredienteVO adicionado = new IngredienteVO();
                for(int i = 0 ; i<jAdicionados.length() ; i++){
                    JSONObject jAdicionado = jAdicionados.getJSONObject(i);

                    adicionado = gson.fromJson(jAdicionado.toString(), IngredienteVO.class);

                    adicionados.add(adicionado);
                }

            } catch (JSONException e2) {
                IngredienteVO adicionado = new IngredienteVO();
                JSONObject jAdicionados = jPedidos.getJSONObject("adicionados");
                adicionado = gson.fromJson(jAdicionados.toString(), IngredienteVO.class);

                adicionados.add(adicionado);
            }
        }

        if(jPedidos.has("removidos")) {
            try {
                JSONArray jRemovidos = jPedidos.getJSONArray("removidos");
                IngredienteVO removido = new IngredienteVO();
                for(int i = 0 ; i<jRemovidos.length() ; i++){
                    JSONObject jRemovido = jRemovidos.getJSONObject(i);

                    removido = gson.fromJson(jRemovido.toString(), IngredienteVO.class);

                    removidos.add(removido);
                }

            } catch (JSONException e2) {
                IngredienteVO removido = new IngredienteVO();
                JSONObject jRemovidos = jPedidos.getJSONObject("removidos");
                removido = gson.fromJson(jRemovidos.toString(), IngredienteVO.class);

                removidos.add(removido);
            }
        }

        try {
            JSONArray jIngredienteVOs = jPedidos.getJSONArray("ingredientes");
            IngredienteVO IngredienteVO = new IngredienteVO();
            for(int i = 0 ; i<jIngredienteVOs.length() ; i++){
                JSONObject jIngredient = jIngredienteVOs.getJSONObject(i);

                IngredienteVO = gson.fromJson(jIngredient.toString(), IngredienteVO.class);

                IngredienteVOs.add(IngredienteVO);
            }

        } catch (JSONException e2) {
            JSONObject jIngredient = jPedidos.getJSONObject("ingredientes");
            IngredienteVO IngredienteVO = new IngredienteVO();
            IngredienteVO = gson.fromJson(jIngredient.toString(), IngredienteVO.class);

            IngredienteVOs.add(IngredienteVO);
        }

        ProdutoMesaVO pedido = new ProdutoMesaVO(
                jPedidos.getInt("codigoM")
                , jPedidos.getInt("qtd")
                , new Double(jPedidos.getDouble("precoM")).doubleValue()
                , jPedidos.getString("situacao")
                , jPedidos.getString("horaEntrada")
                , null
                , jPedidos.getBoolean("ativo")
                , removidos
                , adicionados
                , jPedidos.getInt("tempo")
                , new ProdutoVO(
                jPedidos.getInt("codigo")
                , jPedidos.getString("descricao")
                , new Double(jPedidos.getDouble("preco")).doubleValue()
                , tipo
                , IngredienteVOs
        )
        );

        return pedido;

    }

    private ComandaVO buscaComanda(String url) throws IOException, JSONException {

        ArrayList<ProdutoMesaVO> pedidos = new ArrayList<>();

        ComandaVO comandas;

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getComanda = new HttpGet(url);

        HttpResponse response = httpClient.execute(getComanda);

        HttpEntity entity = response.getEntity();
        JSONObject jComandas = new JSONObject(EntityUtils.toString(entity)).getJSONObject("comanda");

        try {
            JSONArray jPedidos = jComandas.getJSONArray("pedidos");
            for(int i = 0 ; i<jPedidos.length() ; i++){
                JSONObject jPedido = jPedidos.getJSONObject(i);
                pedidos.add(produtoFromJson(jPedido));

            }
        }catch(JSONException e1){
            JSONObject jPedido = jComandas.getJSONObject("pedidos");
            pedidos.add(produtoFromJson(jPedido));
        }

        if(jComandas.has("nome")){
            comandas = new ComandaVO(
                    jComandas.getInt("codigo")
                    , jComandas.getInt("mesa")
                    , jComandas.getString("nome")
                    , jComandas.getString("hr_abertura")
                    , jComandas.getInt("telefone")
                    , jComandas.getBoolean("aberto")
                    , pedidos
            );
        }else{
            comandas = new ComandaVO(
                    jComandas.getInt("codigo")
                    , jComandas.getInt("mesa")
                    , null
                    , jComandas.getString("hr_abertura")
                    , jComandas.getInt("telefone")
                    , jComandas.getBoolean("aberto")
                    , pedidos
            );
        }

        return comandas;
    }

    public List<ComandaVO> buscaTodasComandas(String url) throws IOException, JSONException {

        ComandaVO comanda;
        List<ComandaVO> comandas = new ArrayList<ComandaVO>();

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getComanda = new HttpGet(url);

        HttpResponse response = httpClient.execute(getComanda);

        HttpEntity entity = response.getEntity();

        JSONObject jsonn  = new JSONObject(EntityUtils.toString(entity));

        JSONObject json = jsonn.getJSONObject("comandas");

        String title;
        JSONArray links = null;

        try{
            links = json.getJSONArray("link");
        } catch (JSONException e){

            JSONObject linkk = json.getJSONObject("link");

            title = linkk.getString("@title");

            comanda = this.buscaComanda(url + title.replace(" ", "%20"));

            if(comanda.isAberto()) {
                comandas.add(comanda);
            }
            return comandas;
        }

        for(int i = 0 ; i<links.length() ; i++){
            JSONObject link = links.getJSONObject(i);

            title = link.getString("@title");

            comanda = this.buscaComanda(url + title.replace(" ", "%20"));
            if(comanda.isAberto()) {
                comandas.add(comanda);
            }
        }
        return comandas;
    }

    public List<ComandaVO> buscaTodasComandaMesas(String url, int mesa) throws IOException, JSONException {

        ComandaVO comanda;
        List<ComandaVO> comandas = new ArrayList<ComandaVO>();

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getComanda = new HttpGet(url+"relatorioTel/"+mesa);

        HttpResponse response = httpClient.execute(getComanda);

        HttpEntity entity = response.getEntity();

        JSONObject jsonn  = new JSONObject(EntityUtils.toString(entity));

        String title;
        JSONArray links = null;
        if (!jsonn.toString().equals("{\"comandas\":\"\"}")) {
            JSONObject json = jsonn.getJSONObject("comandas");
            try {
                links = json.getJSONArray("link");
            } catch (JSONException e) {

                JSONObject linkk = json.getJSONObject("link");

                title = linkk.getString("@title");

                comanda = this.buscaComanda(url + title.replace(" ", "%20"));

                if (comanda.isAberto()) {
                    comandas.add(comanda);
                }
                return comandas;
            }

            for (int i = 0; i < links.length(); i++) {
                JSONObject link = links.getJSONObject(i);

                title = link.getString("@title");

                comanda = this.buscaComanda(url + title.replace(" ", "%20"));
                if (comanda.isAberto()) {
                    comandas.add(comanda);
                }
            }
        }
        return comandas;
    }

    private static int pegaID(String url){

        int posicao = -1;
        String id = "";

        for(int i = (url.length()-1) ; i>=0 ; i--){
            if(url.charAt(i) == '/'){
                posicao = i;
                break;
            }
        }

        if(posicao > 0){
            for(int i = (posicao+1) ; i<url.length() ; i++){
                id = id+url.charAt(i);
            }
        }
        return Integer.parseInt(id);

    }

    public ComandaVO criaComanda (String url, ComandaVO comanda) throws IOException, JSONException {

        HttpClient httpClient = new DefaultHttpClient();

        HttpPost postComanda = new HttpPost(url);

        JSONObject jComanda = new JSONObject();

        jComanda.put("codigo", comanda.getCodigo());
        jComanda.put("mesa", comanda.getMesa());

        if(comanda.getPedidos() != null && !comanda.getPedidos().isEmpty()){
            if(comanda.getPedidos().size() > 1) {
                JSONArray jPedidos = new JSONArray();
                for (ProdutoMesaVO produtoMesa : comanda.getPedidos()) {

                    jPedidos.put(this.jsonFromProdutoMesa(produtoMesa));
                }
                jComanda.put("pedidos", jPedidos);
            }else{
                jComanda.put("pedidos", this.jsonFromProdutoMesa(comanda.getPedidos().get(0)));
            }
        }

        JSONObject Comanda = new JSONObject();

        Comanda.put("comanda",jComanda);
        StringEntity se = new StringEntity(Comanda.toString());
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        postComanda.setEntity(se);

        HttpResponse response = httpClient.execute(postComanda);
        String location = new String();
        for(Header header : response.getHeaders("Location")) {
            location = header.getValue();
        }
        return buscaComanda(url + pegaID(location));

    }

    public void atualizaComanda (String url, ComandaVO comanda) throws JSONException, IOException {

        HttpClient httpClient = new DefaultHttpClient();

        HttpPut putComanda = new HttpPut(url+comanda.getCodigo());

        JSONObject jComanda = new JSONObject();

        jComanda.put("codigo", comanda.getCodigo());
        jComanda.put("mesa", comanda.getMesa());
        jComanda.put("hr_abertura", comanda.getHr_abertura());
        jComanda.put("aberto", comanda.isAberto());
        if(comanda.getPedidos() != null && !comanda.getPedidos().isEmpty()){
            if(comanda.getPedidos().size() > 1) {
                JSONArray jPedidos = new JSONArray();
                for (ProdutoMesaVO produtoMesa : comanda.getPedidos()) {

                    jPedidos.put(this.jsonFromProdutoMesa(produtoMesa));
                }
                jComanda.put("pedidos", jPedidos);
            }else{
                jComanda.put("pedidos", this.jsonFromProdutoMesa(comanda.getPedidos().get(0)));
            }
        }

        JSONObject Comanda = new JSONObject();

        Comanda.put("comanda",jComanda);
        StringEntity se = new StringEntity(Comanda.toString());
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        putComanda.setEntity(se);

        httpClient.execute(putComanda);

    }

}
