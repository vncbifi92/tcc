package tcc_zanza.no_ip.biz.tcc_garcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ComandaWS;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.IngredienteWS;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ProdutoWS;

public class IndexActivity extends Activity {

    private String URLComanda;
    private String URLProduto;
    private String URLIngrediente;
    private String URLProdutoMesa;
    private String URLProdutoTipo;
    private String ip;

    private List<Button>botoes = new ArrayList<>();

    private static List<ComandaVO> comandas = new ArrayList<>();

    private Button atualiza;

    public static void addComanda(ComandaVO comanda){
        IndexActivity.comandas.add(comanda);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        atualiza = (Button)findViewById(R.id.atualiza_index);
        ip = (String) getIntent().getSerializableExtra("ip");

        URLComanda = "http://"+ip+":8080/TCC_RS/comandas/";
        URLProduto = "http://"+ip+":8080/TCC_RS/produtos/";
        URLIngrediente = "http://"+ip+":8080/TCC_RS/ingredientes/";
        URLProdutoMesa = "http://"+ip+":8080/TCC_RS/produtos_mesas/";
        URLProdutoTipo = "http://"+ip+":8080/TCC_RS/produtos_tipo/";


        new ProdutoWS(/*this, */URLProduto, 1).execute();

        new IngredienteWS(/*produto, listaAdicionados, this,*/ URLIngrediente, 1).execute();

        atualiza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comandas = new ArrayList<>();
                caregaBotoes();
                carregaMesas();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        comandas = new ArrayList<>();
        caregaBotoes();
        carregaMesas();
    }

    private void caregaBotoes(){
        Button mesa_1 = (Button)findViewById(R.id.mesa_1);
        botoes.add(mesa_1);
        Button mesa_2 = (Button)findViewById(R.id.mesa_2);
        botoes.add(mesa_2);
        Button mesa_3 = (Button)findViewById(R.id.mesa_3);
        botoes.add(mesa_3);
        Button mesa_4 = (Button)findViewById(R.id.mesa_4);
        botoes.add(mesa_4);
        Button mesa_5 = (Button)findViewById(R.id.mesa_5);
        botoes.add(mesa_5);
        Button mesa_6 = (Button)findViewById(R.id.mesa_6);
        botoes.add(mesa_6);
        Button mesa_7 = (Button)findViewById(R.id.mesa_7);
        botoes.add(mesa_7);
        Button mesa_8 = (Button)findViewById(R.id.mesa_8);
        botoes.add(mesa_8);
        Button mesa_9 = (Button)findViewById(R.id.mesa_9);
        botoes.add(mesa_9);
        Button mesa_10 = (Button)findViewById(R.id.mesa_10);
        botoes.add(mesa_10);
        Button mesa_11 = (Button)findViewById(R.id.mesa_11);
        botoes.add(mesa_11);
        Button mesa_12 = (Button)findViewById(R.id.mesa_12);
        botoes.add(mesa_12);
        Button mesa_13 = (Button)findViewById(R.id.mesa_13);
        botoes.add(mesa_13);
        Button mesa_14 = (Button)findViewById(R.id.mesa_14);
        botoes.add(mesa_14);
        Button mesa_15 = (Button)findViewById(R.id.mesa_15);
        botoes.add(mesa_15);
        Button mesa_16 = (Button)findViewById(R.id.mesa_16);
        botoes.add(mesa_16);
        Button mesa_17 = (Button)findViewById(R.id.mesa_17);
        botoes.add(mesa_17);
        Button mesa_18 = (Button)findViewById(R.id.mesa_18);
        botoes.add(mesa_18);
        Button mesa_19 = (Button)findViewById(R.id.mesa_19);
        botoes.add(mesa_19);
        Button mesa_20 = (Button)findViewById(R.id.mesa_20);
        botoes.add(mesa_20);
        Button mesa_21 = (Button)findViewById(R.id.mesa_21);
        botoes.add(mesa_21);
        Button mesa_22 = (Button)findViewById(R.id.mesa_22);
        botoes.add(mesa_22);
        Button mesa_23 = (Button)findViewById(R.id.mesa_23);
        botoes.add(mesa_23);
        Button mesa_24 = (Button)findViewById(R.id.mesa_24);
        botoes.add(mesa_24);
        Button mesa_25 = (Button)findViewById(R.id.mesa_25);
        botoes.add(mesa_25);
        Button mesa_26 = (Button)findViewById(R.id.mesa_26);
        botoes.add(mesa_26);
        Button mesa_27 = (Button)findViewById(R.id.mesa_27);
        botoes.add(mesa_27);
        Button mesa_28 = (Button)findViewById(R.id.mesa_28);
        botoes.add(mesa_28);
        Button mesa_29 = (Button)findViewById(R.id.mesa_29);
        botoes.add(mesa_29);
        Button mesa_30 = (Button)findViewById(R.id.mesa_30);
        botoes.add(mesa_30);
        Button mesa_31 = (Button)findViewById(R.id.mesa_31);
        botoes.add(mesa_31);
        Button mesa_32 = (Button)findViewById(R.id.mesa_32);
        botoes.add(mesa_32);
        Button mesa_33 = (Button)findViewById(R.id.mesa_33);
        botoes.add(mesa_33);
        Button mesa_34 = (Button)findViewById(R.id.mesa_34);
        botoes.add(mesa_34);
        Button mesa_35 = (Button)findViewById(R.id.mesa_35);
        botoes.add(mesa_35);
        Button mesa_36 = (Button)findViewById(R.id.mesa_36);
        botoes.add(mesa_36);
        Button mesa_37 = (Button)findViewById(R.id.mesa_37);
        botoes.add(mesa_37);
        Button mesa_38 = (Button)findViewById(R.id.mesa_38);
        botoes.add(mesa_38);
        Button mesa_39 = (Button)findViewById(R.id.mesa_39);
        botoes.add(mesa_39);
        Button mesa_40 = (Button)findViewById(R.id.mesa_40);
        botoes.add(mesa_40);

        for(Button botao : botoes) {
            botao.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.mesavazia, 0, 0);
            botao.setOnClickListener(abreMesa());
        }
    }

    public View.OnClickListener abreMesa() {
        return new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Button botao = (Button)findViewById(v.getId());

                if(IndexActivity.comandas != null && !IndexActivity.comandas.isEmpty()) {
                    for (ComandaVO comanda : IndexActivity.comandas) {

                        if (botao.getText().equals(comanda.getMesa() + "")) {
                            Intent intent = new Intent(IndexActivity.this, PedidosDaMesa.class);

                            intent.putExtra("comanda", comanda);
                            intent.putExtra("URLProduto", URLProduto);
                            intent.putExtra("URLComanda", URLComanda);
                            intent.putExtra("URLIngrediente", URLIngrediente);
                            intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                            intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                            startActivity(intent);
                            return;

                        }
                    }
                }

                ComandaVO comanda = new ComandaVO();

                comanda.setMesa(Integer.parseInt(botao.getText().toString()));
                Intent intent = new Intent(IndexActivity.this, PedidosDaMesa.class);

                intent.putExtra("comanda", comanda);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);

                startActivity(intent);
                return;
            }
        };
    }

    private void carregaMesas (){
        ComandaWS ws = new ComandaWS(this, URLComanda, 2, botoes);

        ws.execute();

    }

}