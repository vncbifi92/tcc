package tcc_zanza.no_ip.biz.tcc_garcom.vo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by VINICIUS on 10/10/2015.
 */
public class ProdutoVO implements Serializable {

    private int codigo;
    private String descricao;
    private double preco;
    private ProdutoTipoVO tipo;
    private ArrayList<IngredienteVO> ingredientes;

    public ProdutoVO() {
    }

    public ProdutoVO(int codigo, String descricao, double preco,
                     ProdutoTipoVO tipo, ArrayList<IngredienteVO> ingredientes) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.preco = preco;
        this.tipo = tipo;
        this.ingredientes = ingredientes;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public ProdutoTipoVO getTipo() {
        return tipo;
    }

    public void setTipo(ProdutoTipoVO tipo) {
        this.tipo = tipo;
    }

    public ArrayList<IngredienteVO> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(ArrayList<IngredienteVO> ingredientes) {
        this.ingredientes = ingredientes;
    }

    @Override
    public String toString() {
        return "ProdutoVO [codigo=" + codigo + ", descricao=" + descricao + ", preco=" + preco + ", tipo=" + tipo
                + ", ingredientes=" + ingredientes + "]";
    }

}
