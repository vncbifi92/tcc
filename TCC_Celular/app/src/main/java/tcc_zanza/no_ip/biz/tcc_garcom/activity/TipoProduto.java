package tcc_zanza.no_ip.biz.tcc_garcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;

public class TipoProduto extends Activity {

    private String URLComanda;
    private String URLProduto;
    private String URLIngrediente;
    private String URLProdutoMesa;
    private String URLProdutoTipo;
    private ComandaVO comanda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_produto);

        Button confirmar = (Button)findViewById(R.id.activity_inicio_confirmar);
        Button pastel_especial = (Button) findViewById(R.id.activity_inicio_pastel_especial);
        Button pastel = (Button) findViewById(R.id.activity_inicio_pastel);
        Button voltar = (Button) findViewById(R.id.activity_inicio_voltar);
        URLProduto = (String)getIntent().getSerializableExtra("URLProduto");
        URLIngrediente = (String)getIntent().getSerializableExtra("URLIngrediente");
        URLProdutoMesa = (String)getIntent().getSerializableExtra("URLProdutoMesa");
        URLComanda = (String)getIntent().getSerializableExtra("URLComanda");
        URLProdutoTipo = (String)getIntent().getSerializableExtra("URLProdutoTipo");
        //comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");

        pastel_especial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TipoProduto.this, Produtos.class);
                intent.putExtra("comanda", comanda);
                intent.putExtra("tipo", "Pastel - Especial");
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                startActivity(intent);
            }
        });

        pastel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TipoProduto.this, Produtos.class);
                intent.putExtra("tipo", "Pastel");
                intent.putExtra("comanda", comanda);
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                startActivity(intent);
            }
        });

        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TipoProduto.this, PedidosDaMesa.class);
                intent.putExtra("comanda", comanda);
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                startActivity(intent);
            }
        });

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");
    }
}
