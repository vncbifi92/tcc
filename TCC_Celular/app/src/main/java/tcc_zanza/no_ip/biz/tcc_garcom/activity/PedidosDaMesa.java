package tcc_zanza.no_ip.biz.tcc_garcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.ProdutoMesaAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ComandaWS;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ProdutoMesaWS;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ProdutoTipoWS;

public class PedidosDaMesa extends Activity {

    private String URLComanda;
    private String URLProduto;
    private String URLIngrediente;
    private String URLProdutoMesa;
    private String URLProdutoTipo;
    private ComandaVO comanda;
    private ListView listaPrdidos;
    private Button finalizar;
    private TextView textPreco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_da_mesa);

        TextView textMesa = (TextView)findViewById(R.id.activity_pedidos_da_mesa_texto_mesa);

        textPreco = (TextView)findViewById(R.id.activity_pedidos_da_mesa_texto_valor);
        Button voltar = (Button)findViewById(R.id.activity_pedidos_da_mesa_botao_voltar);
        listaPrdidos = (ListView)findViewById(R.id.activity_pedidos_da_mesa_lista_de_pedidos);
        Button inserir = (Button)findViewById(R.id.activity_pedidos_da_mesa_botao_inseir_pedido);
        finalizar = (Button)findViewById(R.id.activity_pedidos_da_mesa_botao_finalilzar_pedido);
        comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");
        URLProduto = (String)getIntent().getSerializableExtra("URLProduto");
        URLIngrediente = (String)getIntent().getSerializableExtra("URLIngrediente");
        URLComanda = (String)getIntent().getSerializableExtra("URLComanda");
        URLProdutoMesa = (String)getIntent().getSerializableExtra("URLProdutoMesa");
        URLProdutoTipo = (String)getIntent().getSerializableExtra("URLProdutoTipo");
        textMesa.setText("Mesa " + comanda.getMesa());

        textPreco.setText("Valor: R$" + comanda.getPreco());

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        inserir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PedidosDaMesa.this, TipoProduto.class);
                intent.putExtra("comanda", comanda);
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                startActivity(intent);
                finish();
            }
        });
        listaPrdidos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ProdutoMesaVO remove = (ProdutoMesaVO)parent.getItemAtPosition(position);
                int pos = -1;
                for(int i = 0 ; i<comanda.getPedidos().size() ; i++){
                    ProdutoMesaVO produto = comanda.getPedidos().get(i);
                    if(produto.equals(remove)){
                        pos = i;
                        break;
                    }
                }

                if(comanda.getPedidos().get(pos).getQtd() == 1) {
                    comanda.getPedidos().remove(pos);
                }else{
                    comanda.getPedidos().get(pos).setQtd(comanda.getPedidos().get(pos).getQtd()-1);
                }
                atualizaPreco();
                onResume();
                finalizar.setEnabled(true);
                return false;
            }
        });
        finalizar.setEnabled(false);
        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*ProdutoMesaWS wsProdutoMesa = new ProdutoMesaWS(PedidosDaMesa.this, URLProdutoMesa, 1);

                for(int i = 0 ; i < comanda.getPedidos().size() ; i++){
                    wsProdutoMesa.setEntidade(comanda.getPedidos().get(i));
                    wsProdutoMesa.execute();
                    comanda.getPedidos().remove(i);
                    comanda.getPedidos().add(wsProdutoMesa.getEntidade());
                }*/
                boolean novo = false;

                if (comanda.getPedidos() != null && !comanda.getPedidos().isEmpty()) {
                    for (ProdutoMesaVO produto : comanda.getPedidos()) {

                        if (produto.getTempo() != 0) {
                            novo = true;
                            break;
                        }
                    }
                    if (comanda.getCodigo() == 0) {
                        ComandaWS wsComanda = new ComandaWS(PedidosDaMesa.this, URLComanda, 3, null);
                        wsComanda.setEntidade(comanda);
                        wsComanda.serUTLProdutoMesa(URLProdutoMesa);
                        wsComanda.execute();

                        comanda = wsComanda.getEntidade();
                    } else {
                        ComandaWS wsComanda = new ComandaWS(PedidosDaMesa.this, URLComanda, 4, null);
                        wsComanda.setEntidade(comanda);
                        wsComanda.serUTLProdutoMesa(URLProdutoMesa);
                        wsComanda.execute();

                        comanda = wsComanda.getEntidade();
                    }
                }

                finish();

            }
        });
    }


    private void tempo(ComandaVO comanda){

        int qdtPastel = 0;
        int qtdPastelEspecial = 0;

        for(ProdutoMesaVO produto : comanda.getPedidos()){
            if(produto.getTipo().getDescricao().equals("Pastel - Especial") && produto.getCodigoM() == 0){
                qtdPastelEspecial = qtdPastelEspecial+produto.getQtd();
            }
            if(produto.getTipo().getDescricao().equals("Pastel")&& produto.getCodigoM() == 0){
                qdtPastel = qdtPastel + produto.getQtd();
            }
        }



        ProdutoMesaWS wsMesa = new ProdutoMesaWS(this, URLProdutoMesa, 2);
        wsMesa.setQtd(qdtPastel, qtdPastelEspecial);
        wsMesa.setURLProdutoTipo(URLProdutoTipo);
        wsMesa.execute();

    }

    private void atualizaPreco(){
        textPreco.setText("Valor: R$" + comanda.getPreco());
    }

    @Override
    protected void onResume() {
        super.onResume();
        atualizaPreco();
        TextView tempo = (TextView)findViewById(R.id.tempo);
        comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");

        tempo.setText(0 + "");

        boolean novo = false;

        if(comanda.getPedidos() != null && !comanda.getPedidos().isEmpty()) {
            for(ProdutoMesaVO produto : comanda.getPedidos()){
                if(produto.getCodigoM() == 0){
                    novo = true;
                    finalizar.setEnabled(true);
                    break;
                }
            }
            if(novo){
                this.tempo(comanda);
            }else{
                TextView view = (TextView)this.findViewById(R.id.tempo);
                Integer tempoI = 0;
                for(ProdutoMesaVO produto : comanda.getPedidos()){
                    if(produto.getTempo() > tempoI) {
                        tempoI = produto.getTempo();
                    }
                }
                view.setText(tempoI.toString());
            }
        }


        carregaLista();
    }

    private  void carregaLista(){
        List<ProdutoMesaVO> produtos = new ArrayList<>();
        if(comanda != null) {
            for(ProdutoMesaVO produtoMesa : comanda.getPedidos()) {
                List<IngredienteVO> ingredientes = new ArrayList<>();
                if(produtoMesa.isAtivo()) {
                    if (produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
                        for (IngredienteVO ingrediente : produtoMesa.getAdicionados()) {
                            if(!ingrediente.getDescricao().contains("Com ")) {
                                ingrediente.setDescricao("Com " + ingrediente.getDescricao());
                            }
                            ingredientes.add(ingrediente);
                        }
                    }

                    if (produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
                        for (IngredienteVO ingrediente : produtoMesa.getRemovidos()) {
                            if(!ingrediente.getDescricao().contains("Sem ")) {
                                ingrediente.setDescricao("Sem " + ingrediente.getDescricao());
                            }
                            ingredientes.add(ingrediente);
                        }
                    }
                    produtos.add(produtoMesa);
                }
            }

            ProdutoMesaAdapter adapter = new ProdutoMesaAdapter(produtos, this);

            listaPrdidos.setAdapter(adapter);

        }

    }

}
