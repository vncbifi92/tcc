package tcc_zanza.no_ip.biz.tcc_garcom.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.activity.IndexActivity;
import tcc_zanza.no_ip.biz.tcc_garcom.activity.PedidosDaMesa;
import tcc_zanza.no_ip.biz.tcc_garcom.dao.ComandaDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.dao.ProdutoMesaDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 10/10/2015.
 */
public class ComandaWS extends AsyncTask<Void, Void, List<ComandaVO>> {

    private String url;
    private int acao;
    private ProgressDialog progress;
    private Activity activity;
    private List<Button> botoes = null;
    private ComandaVO comanda;
    private String URLProdutoMesa = null;

    public ComandaWS( Activity activity, String url, int acao, List<Button> botoes ){
        this.acao = acao;
        this.url = url;
        this.activity = activity;
        this.botoes = botoes;
    }

    public ComandaVO getEntidade(){
        return this.comanda;
    }

    public void setEntidade(ComandaVO comanda){

        this.comanda = comanda;

    }

    public void serUTLProdutoMesa(String URLProdutoMesa){
        this.URLProdutoMesa = URLProdutoMesa;
    }

    @Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(activity, "Aguarde...", "Carregando dados do servidor.");
    }

    @Override
    protected List<ComandaVO> doInBackground(Void... params) {
        List<ComandaVO> comandas = new ArrayList<>();

        ComandaDAO dao = new ComandaDAO();

        switch (acao){
            case 1:
                try {

                    comandas = dao.buscaTodasComandas(url);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;

            case 2:
                try {
                    for(int i = 1 ; i<41 ; i++) {
                        comandas.addAll(dao.buscaTodasComandaMesas(url, i));
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;

            case 3:
                if(this.comanda != null && URLProdutoMesa != null && !URLProdutoMesa.isEmpty()) {
                    ProdutoMesaDAO daoPM = new ProdutoMesaDAO();
                    ArrayList<ProdutoMesaVO> Lista = new ArrayList<ProdutoMesaVO>();
                    try {

                        for(int i = 0 ; i < comanda.getPedidos().size() ; i++){
                            Lista.add(daoPM.criaProdutosMesa(URLProdutoMesa, comanda.getPedidos().get(i)));
                        }

                        comanda.setPedidos(Lista);

                        comandas.add(dao.criaComanda(url, this.comanda));
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 4:
                ProdutoMesaDAO daoPM = new ProdutoMesaDAO();
                ArrayList<ProdutoMesaVO> Lista = new ArrayList<ProdutoMesaVO>();
                try {
                    if(this.comanda != null && this.comanda.getCodigo() != 0 && URLProdutoMesa != null && !URLProdutoMesa.isEmpty()) {
                        for(int i = 0 ; i < comanda.getPedidos().size() ; i++){
                            if(comanda.getPedidos().get(i).getTempo() == 0) {
                                Lista.add(daoPM.criaProdutosMesa(URLProdutoMesa, comanda.getPedidos().get(i)));
                                comanda.getPedidos().remove(i);
                            }
                        }
                        comanda.getPedidos().addAll(Lista);
                    }

                    dao.atualizaComanda(url, comanda);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }

        return comandas;
    }

    @Override
    protected void onPostExecute(List<ComandaVO> comandas) {
        progress.dismiss();

        if((acao == 1 || acao == 2) && botoes != null && !botoes.isEmpty()) {
            for (ComandaVO comanda : comandas) {
                String texto = comanda.getMesa() + "";

                for (Button botao : botoes) {
                    ArrayList<ProdutoMesaVO> pedidos = new ArrayList<>();
                    if (botao.getText().equals(texto) && comanda.isAberto()) {
                        for(ProdutoMesaVO pedido : comanda.getPedidos()){
                            if(pedido.getSituacao().equals("Aguardando Producao")){
                                pedidos.add(pedido);
                            }
                        }
                        comanda.setPedidos(pedidos);
                        IndexActivity.addComanda(comanda);
                        mudaUmBotao(botao.getId());
                    }
                }
            }
        }

        if(acao == 3){
            if(comandas.size()>0) {
                this.setEntidade(comandas.get(0));
            }
        }
    }

    private void mudaUmBotao(int id){
        for(Button botao : botoes) {
            if(botao.getId() == id) {
                botao.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.mesacheia, 0, 0);
            }
        }
    }
}
