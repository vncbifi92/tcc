package tcc_zanza.no_ip.biz.tcc_garcom.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;

/**
 * Created by VINICIUS on 10/12/2015.
 */
public class IngredienteAdapter extends BaseAdapter {

    private List<IngredienteVO> ingredientes;
    private Activity act;

    public IngredienteAdapter(List<IngredienteVO> ingredientes, Activity act){

        this.act = act;
        this.ingredientes = ingredientes;

    }

    @Override
    public int getCount() {
        return ingredientes.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredientes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ingredientes.get(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        IngredienteVO ingrediente = (IngredienteVO) getItem(position);

        LayoutInflater inflater = act.getLayoutInflater();

        View linha = inflater.inflate(R.layout.item_lista_ingredientes, null);

        TextView descricao = (TextView)linha.findViewById(R.id.lista_ingrediente_descricao);

        descricao.setText(ingrediente.getDescricao());

        return linha;

    }

}
