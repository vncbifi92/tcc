package tcc_zanza.no_ip.biz.tcc_garcom.dao;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;

/**
 * Created by VINICIUS on 10/19/2015.
 */
public class IngredienteDAO {

    public IngredienteVO buscaIngrediente(String url) throws IOException, JSONException {

        IngredienteVO ingrediente;

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getIngred = new HttpGet(url);

        HttpResponse response = httpClient.execute(getIngred);

        HttpEntity entity = response.getEntity();

        Gson gson = new Gson();

        JSONObject json = new JSONObject(EntityUtils.toString(entity)).getJSONObject("ingrediente");


        ingrediente = gson.fromJson(json.toString(), IngredienteVO.class);


        return ingrediente;
    }

    public List<IngredienteVO> buscaTodosIngredientes(String url) throws IOException, JSONException {

        IngredienteVO ingrediente;

        List<IngredienteVO> ingredientes = new ArrayList<IngredienteVO>();

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getIngred = new HttpGet(url);

        HttpResponse response = httpClient.execute(getIngred);

        HttpEntity entity = response.getEntity();

        JSONObject json = new JSONObject(EntityUtils.toString(entity));

        JSONObject jsonIngredientes = json.getJSONObject("ingredientes");

        String title = new String();
        JSONArray links = null;
        try {
            links = jsonIngredientes.getJSONArray("link");
        } catch (JSONException e) {
            JSONObject linkk = jsonIngredientes.getJSONObject("link");

            title = linkk.getString("@title");

            ingrediente = this.buscaIngrediente(url + title.replace(" ", "%20"));

            ingredientes.add(ingrediente);
            return ingredientes;
        }

        for(int i = 0 ; i<links.length() ; i++){

            JSONObject link = links.getJSONObject(i);

            title = link.getString("@title");

            ingrediente = this.buscaIngrediente(url + title.replace(" ", "%20"));

            ingredientes.add(ingrediente);
        }
        return ingredientes;
    }
}
