package tcc_zanza.no_ip.biz.tcc_garcom.dao;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoTipoVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;

/**
 * Created by VINICIUS on 10/16/2015.
 */
public class ProdutoDAO {

    public ProdutoVO buscaProduto(String url) throws IOException, JSONException {


        ProdutoVO produto;
        ProdutoTipoVO tipo;
        ArrayList<IngredienteVO> IngredienteVOs = new ArrayList<>();

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getproduto = new HttpGet(url);

        HttpResponse response = httpClient.execute(getproduto);

        HttpEntity entity = response.getEntity();

        Gson gson = new Gson();

        JSONObject jPedidos = new JSONObject(EntityUtils.toString(entity)).getJSONObject("produto");

        JSONObject jTipo = jPedidos.getJSONObject("tipo");

        tipo = gson.fromJson(jTipo.toString(), ProdutoTipoVO.class);

        try {
            JSONArray jIngredienteVOs = jPedidos.getJSONArray("ingredientes");
            IngredienteVO IngredienteVO = new IngredienteVO();
            for(int i = 0 ; i<jIngredienteVOs.length() ; i++){
                JSONObject jIngredient = jIngredienteVOs.getJSONObject(i);

                IngredienteVO = gson.fromJson(jIngredient.toString(), IngredienteVO.class);

                IngredienteVOs.add(IngredienteVO);
            }

        } catch (JSONException e2) {
            JSONObject jIngredient = jPedidos.getJSONObject("ingredientes");
            IngredienteVO IngredienteVO = new IngredienteVO();
            IngredienteVO = gson.fromJson(jIngredient.toString(), IngredienteVO.class);

            IngredienteVOs.add(IngredienteVO);
        }

        produto = new ProdutoVO(
        jPedidos.getInt("codigo")
        , jPedidos.getString("descricao")
        , new Double(jPedidos.getDouble("preco")).doubleValue()
        , tipo
        , IngredienteVOs );

        return produto;

    }

    public List<ProdutoVO> buscaTodosProdutos(String url) throws IOException, JSONException {

        ProdutoVO produto;

        List<ProdutoVO> produtos = new ArrayList<ProdutoVO>();

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getProduto = new HttpGet(url);

        HttpResponse response = httpClient.execute(getProduto);

        HttpEntity entity = response.getEntity();

        JSONObject json = new JSONObject(EntityUtils.toString(entity));

        JSONObject jsonIngredientes = json.getJSONObject("produtos");

        String title = new String();
        JSONArray links = null;
        try {
            links = jsonIngredientes.getJSONArray("link");
        } catch (JSONException e) {
            JSONObject linkk = jsonIngredientes.getJSONObject("link");

            title = linkk.getString("@title");

            produto = this.buscaProduto(url + title.replace(" ", "%20"));

            produtos.add(produto);
            return produtos;
        }

        for(int i = 0 ; i<links.length() ; i++){

            JSONObject link = links.getJSONObject(i);

            title = link.getString("@title");

            produto = this.buscaProduto(url + title.replace(" ", "%20"));

            produtos.add(produto);
        }
        return produtos;
    }

}
