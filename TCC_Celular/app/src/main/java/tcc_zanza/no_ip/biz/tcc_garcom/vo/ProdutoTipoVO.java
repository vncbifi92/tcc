package tcc_zanza.no_ip.biz.tcc_garcom.vo;

import java.io.Serializable;

/**
 * Created by VINICIUS on 10/10/2015.
 */
public class ProdutoTipoVO implements Serializable {

    private int codigo;
    private String descricao;
    private int tempo;

    public ProdutoTipoVO() {
    }

    public ProdutoTipoVO(int codigo, String descricao, int tempo) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.tempo = tempo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    @Override
    public String toString() {
        return "ProdutoTipoVO [codigo=" + codigo + ", descricao=" + descricao + ", tempo=" + tempo + "]";
    }

}
