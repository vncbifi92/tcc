package tcc_zanza.no_ip.biz.tcc_garcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.AdicionadoAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.RemovidoAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.IngredienteWS;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ProdutoMesaWS;

public class CriaProdutoDaMesa extends Activity {
    private static List<IngredienteVO> removidos = new ArrayList<>();
    private static List<IngredienteVO> adicionados = new ArrayList<>();
    private static List<IngredienteVO> ingredientes = new ArrayList<>();
    private String URLComanda;
    private String URLProduto;
    private String URLIngrediente;
    private String URLProdutoMesa;
    private String URLProdutoTipo;
    private ComandaVO comanda;
    private ProdutoVO produto;
    private ListView listaAdicionados;
    private ListView listaRemovidos;
    private ProdutoMesaVO novoProdutoMesa = new ProdutoMesaVO();


    public static List getRemovidos(){
        return removidos;
    }
    public static List getAdicionados(){
        return adicionados;
    }

    public static void addIngredientes(List<IngredienteVO> ingrediente){
        ingredientes = ingrediente;
    }

    public static void addRemovido(IngredienteVO ingrediente){
        for(IngredienteVO removido : removidos) {
            if(removido.getDescricao().equals(ingrediente.getDescricao())){
                removidos.remove(removido);
                return;
            }
        }
        removidos.add(ingrediente);
    }

    public static void addAdicionado(IngredienteVO ingrediente){
        for(IngredienteVO adicionado : adicionados) {
            if(adicionado.getDescricao().equals(ingrediente.getDescricao())){
                adicionados.remove(adicionado);
                return;
            }
        }
        adicionados.add(ingrediente);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adicionados = new ArrayList<>();
        removidos = new ArrayList<>();
        //ingredientes = new ArrayList<>();
        setContentView(R.layout.activity_cria_produto_da_mesa);
        Button voltar = (Button)findViewById(R.id.voltar);
        Button criar = (Button)findViewById(R.id.inserir_pedido);
        URLProduto = (String)getIntent().getSerializableExtra("URLProduto");
        URLIngrediente = (String)getIntent().getSerializableExtra("URLIngrediente");
        URLProdutoMesa = (String)getIntent().getSerializableExtra("URLProdutoMesa");
        URLComanda = (String)getIntent().getSerializableExtra("URLComanda");
        URLProdutoTipo = (String)getIntent().getSerializableExtra("URLProdutoTipo");
        comanda = (ComandaVO)getIntent().getSerializableExtra("comanda");
        listaAdicionados = (ListView)findViewById(R.id.lista_adicionar);
        listaRemovidos = (ListView)findViewById(R.id.lista_remover);

        produto = (ProdutoVO)getIntent().getSerializableExtra("produto");

        Button adicionar = (Button)findViewById(R.id.adiciona_produto);

        Button subtrair = (Button)findViewById(R.id.subtrai_produto);

        final TextView quantidade = (TextView)findViewById(R.id.quantidade_produto);

        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantidadeInt = Integer.parseInt(quantidade.getText().toString());
                quantidade.setText((quantidadeInt + 1) + "");

            }
        });

        subtrair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(quantidade.getText().toString()) > 0) {
                    quantidade.setText((Integer.parseInt(quantidade.getText().toString()) - 1)+"");
                }
            }
        });

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        criar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Integer.parseInt(quantidade.getText().toString()) > 0) {
                    double preco = 0;
                    if (novoProdutoMesa.getAdicionados() != null && !novoProdutoMesa.getAdicionados().isEmpty()) {
                        for (IngredienteVO ingrediente : novoProdutoMesa.getAdicionados()) {
                            preco = ingrediente.getPreco();
                        }
                    }

                    novoProdutoMesa = new ProdutoMesaVO();
                    novoProdutoMesa.setAdicionados(adicionados);
                    novoProdutoMesa.setRemovidos(removidos);
                    novoProdutoMesa.setProduto(produto);
                    novoProdutoMesa.setSituacao("Aguardando Producao");
                    novoProdutoMesa.setAtivo(true);
                    novoProdutoMesa.setPrecoM(preco + novoProdutoMesa.getPreco());
                    novoProdutoMesa.setTempo(0);

                    novoProdutoMesa.setQtd(Integer.parseInt(quantidade.getText().toString()));
                    /*ProdutoMesaWS ws = new ProdutoMesaWS(CriaProdutoDaMesa.this, URLProdutoMesa, 1);
                    ws.setEntidade(novoProdutoMesa);

                    ws.execute();*/
                    comanda.addPedido(novoProdutoMesa);
                    Produtos.addComanda(comanda);
                    finish();

                    /*Intent intent = new Intent(CriaProdutoDaMesa.this, IndexActivity.class);
                    intent.putExtra("ip", ip);
                    intent.putExtra("comanda", comanda);
                    startActivity(intent);*/

                }
            }
        });

        carregaListas();

    }

    private void carregaListas(){

        //new IngredienteWS(/*produto, listaAdicionados,*/ this, URLIngrediente, 1).execute();

        List<IngredienteVO> existentes = new ArrayList<>();

        int tam = ingredientes.size();
        for(int i = 0 ; i<(tam-1) ; i++){
            if(!produto.getIngredientes().contains(ingredientes.get(i)) &&((
                    produto.getTipo().getDescricao().equals("Pastel - Especial")&&
                            ingredientes.get(i).getPreco() == 0.75) ||
                    (produto.getTipo().getDescricao().equals("Pastel")&&
                            ingredientes.get(i).getPreco() == 0.5))){
                existentes.add(ingredientes.get(i));
            }

        }

        AdicionadoAdapter adicionarIngred = new AdicionadoAdapter(existentes, this);

        listaAdicionados.setAdapter(adicionarIngred);

        RemovidoAdapter removerIngred = new RemovidoAdapter(produto.getIngredientes(), this);
        listaRemovidos.setAdapter(removerIngred);

    }
}
