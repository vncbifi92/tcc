package tcc_zanza.no_ip.biz.tcc_garcom.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.activity.CriaProdutoDaMesa;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;

/**
 * Created by VINICIUS on 10/19/2015.
 */
public class AdicionadoAdapter extends BaseAdapter {
    private List<IngredienteVO> ingredientes;
    private Activity act;

    public AdicionadoAdapter(List<IngredienteVO> ingredientes, Activity act){

        this.act = act;
        this.ingredientes = ingredientes;

    }

    @Override
    public int getCount() {
        return ingredientes.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredientes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ingredientes.get(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final IngredienteVO ingrediente = (IngredienteVO) getItem(position);

        LayoutInflater inflater = act.getLayoutInflater();

        View linha = inflater.inflate(R.layout.item_lista_adiciona_item, null);

        TextView descricao = (TextView)linha.findViewById(R.id.lista_adicionado_descricao);
        TextView preco = (TextView)linha.findViewById(R.id.lista_adicionado_preco);

        final CheckBox check = (CheckBox)linha.findViewById(R.id.lista_adiciona_ingrediente);

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CriaProdutoDaMesa.addAdicionado(ingrediente);
            }
        });

        descricao.setText(ingrediente.getDescricao());

        Double precoDouble = ingrediente.getPreco();

        preco.setText(precoDouble.toString());

        if(CriaProdutoDaMesa.getAdicionados().contains(ingrediente)){
            check.setChecked(true);
        }

        return linha;

    }
}
