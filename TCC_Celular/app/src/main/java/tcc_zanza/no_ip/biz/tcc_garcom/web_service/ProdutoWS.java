package tcc_zanza.no_ip.biz.tcc_garcom.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.LabeledIntent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.activity.Produtos;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.ProdutoAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.dao.ProdutoDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;

/**
 * Created by VINICIUS on 10/16/2015.
 */
public class ProdutoWS  extends AsyncTask<Void, Void, List<ProdutoVO>> {

    private String url;
    private int acao;
    private ProgressDialog progress;
    //private Activity activity;
    //private String tipo;

    /*public void setTipo(String tipo){
        this.tipo = tipo;
    }*/

    public ProdutoWS(/*Activity activity,*/ String url, int acao){

        //this.activity = activity;
        this.acao = acao;
        this.url = url;

    }

    /*@Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(activity, "Aguarde..", "Carregando dados do servidor.");
    }*/

    @Override
    protected List<ProdutoVO> doInBackground(Void... params) {
        List<ProdutoVO> produtos = new ArrayList<ProdutoVO>();

        ProdutoDAO dao = new ProdutoDAO();

        switch (acao) {
            case 1:
                try {
                    produtos = dao.buscaTodosProdutos(url);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }

        return produtos;
    }

    @Override
    protected void onPostExecute(List<ProdutoVO> produtos) {
        Produtos.addProdutos(produtos);
        //progress.dismiss();

        /*ListView lista_pastel = (ListView)activity.findViewById(R.id.lista);

        List<ProdutoVO> listaPastel = new ArrayList<>();

        for(ProdutoVO produto : produtos){
            if(produto.getTipo().getDescricao().equals(this.tipo)){
                listaPastel.add(produto);
            }
        }

        if(listaPastel.size() > 0) {
            ProdutoAdapter adapter = new ProdutoAdapter(listaPastel, activity);

            lista_pastel.setAdapter(adapter);
        }*/

    }

}
