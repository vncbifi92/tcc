package tcc_zanza.no_ip.biz.tcc_garcom.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.dao.ProdutoMesaDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.dao.ProdutoTipoDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;

/**
 * Created by VINICIUS on 10/22/2015.
 */
public class ProdutoMesaWS extends AsyncTask<Void, Void, ProdutoMesaVO> {

    private String url;
    private int acao;
    private ProgressDialog progress;
    private Activity activity;
    private ProdutoMesaVO produtoMesa = null;
    private int tempo;
    private int qdtPastel;
    private int qtdPastelEspecial;
    private String URLProdutoTipo;


    public ProdutoMesaWS( Activity activity, String url, int acao ){
        this.acao = acao;
        this.url = url;
        this.activity = activity;
    }

    public ProdutoMesaVO getEntidade(){
        return this.produtoMesa;
    }

    public int getTempo(){
        return this.tempo;
    }
    public void setQtd(int qdtPastel, int qtdPastelEspecial){
        this.qdtPastel = qdtPastel;
        this.qtdPastelEspecial = qtdPastelEspecial;
    }
    private void setTempo(int tempo){
        this.tempo = tempo;
    }

    public void setEntidade(ProdutoMesaVO produtoMesa){
        this.produtoMesa = produtoMesa;
    }

    @Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(activity, "Aguarde..", "Carregando dados do servidor.");
    }

    public void setURLProdutoTipo(String URLProdutoTipo){
        this.URLProdutoTipo = URLProdutoTipo;
    }

    @Override
    protected ProdutoMesaVO doInBackground(Void... params) {
        ProdutoMesaVO produtoMesa = new ProdutoMesaVO();
        ProdutoMesaDAO dao = new ProdutoMesaDAO();

        switch (acao){
            case 1:
                if(this.produtoMesa != null) {
                    try {

                        produtoMesa = dao.criaProdutosMesa(url, this.produtoMesa);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 2:
                try {
                    ProdutoTipoDAO daoTipo = new ProdutoTipoDAO();
                    int tempoPastelEspecial = daoTipo.getTempo(URLProdutoTipo, "Pastel - Especial".replace(" ", "%20"));
                    int tempoPastel = daoTipo.getTempo(URLProdutoTipo, "Pastel");


                    int maxProduto = dao.getTempo(url);
                    this.setTempo((tempoPastelEspecial * qtdPastelEspecial) + (tempoPastel * qdtPastel) + maxProduto);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }

        return produtoMesa;
    }

    @Override
    protected void onPostExecute(ProdutoMesaVO produtoMesa) {
        if(acao == 1) {
            this.setEntidade(produtoMesa);
        }
        if(acao == 2){
            Integer tempoI = this.getTempo();
            TextView view = (TextView)activity.findViewById(R.id.tempo);
            view.setText(tempoI.toString());

        }


        progress.dismiss();

    }

}
