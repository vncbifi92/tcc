package tcc_zanza.no_ip.biz.tcc_garcom.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.activity.Produtos;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;

/**
 * Created by VINICIUS on 10/17/2015.
 */
public class ProdutoAdapter extends BaseAdapter {
    private List<ProdutoVO> produtos;
    private Activity act;
    //private TextView quantidade;

    public ProdutoAdapter(List<ProdutoVO> produtos, Activity act){

        this.produtos = produtos;
        this.act = act;
    }

    @Override
    public int getCount() {
        return produtos.size();
    }

    @Override
    public Object getItem(int position) {
        return produtos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return produtos.get(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ProdutoVO produto = (ProdutoVO)getItem(position);


        LayoutInflater inflater = act.getLayoutInflater();

        View linha = inflater.inflate(R.layout.item_activity_produtos, null);

        TextView descricao = (TextView)linha.findViewById(R.id.descricao_produto);
        descricao.setText(produto.getDescricao());

        TextView preco = (TextView)linha.findViewById(R.id.preco_produto);
        Double precoDouble = produto.getPreco();
        preco.setText(precoDouble.toString());

        return linha;
    }
}
