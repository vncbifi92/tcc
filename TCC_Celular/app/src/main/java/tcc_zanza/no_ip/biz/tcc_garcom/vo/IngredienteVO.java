package tcc_zanza.no_ip.biz.tcc_garcom.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by VINICIUS on 10/10/2015.
 */
public class IngredienteVO implements Serializable {

    private int codigo;
    private String descricao;
    private String unidade;
    private double preco;

    public IngredienteVO(){

    }

    public IngredienteVO(int codigo, String descricao, String unidade,
                       double preco) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.unidade = unidade;
        this.preco = preco;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "IngredienteVO [codigo="+codigo+", descricao=" + descricao + ", unidade=" + unidade
                + ", preco=" + preco +"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredienteVO that = (IngredienteVO) o;
        return Objects.equals(codigo, that.codigo) &&
                Objects.equals(preco, that.preco) &&
                Objects.equals(descricao, that.descricao) &&
                Objects.equals(unidade, that.unidade);
    }

}
