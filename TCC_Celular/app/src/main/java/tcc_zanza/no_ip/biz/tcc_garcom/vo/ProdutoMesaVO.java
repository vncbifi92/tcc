package tcc_zanza.no_ip.biz.tcc_garcom.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by VINICIUS on 10/10/2015.
 */
public class ProdutoMesaVO extends ProdutoVO implements Serializable {

    private int codigoM;
    private int qtd;
    private double precoM;
    private String situacao;
    private String horaEntrada;
    private String horaTermino;
    private boolean ativo;
    private List<IngredienteVO> removidos = new ArrayList<>();
    private List<IngredienteVO> adicionados = new ArrayList<>();
    private int tempo = 0;

    public ProdutoMesaVO() {
        super();
    }

    public ProdutoMesaVO(int codigo, int qtd, double preco, String situacao, String horaEntrada, String horaTermino,
                         boolean ativo, List<IngredienteVO> removidos, List<IngredienteVO> adicionados, int tempo, ProdutoVO produto) {
        super(produto.getCodigo(), produto.getDescricao(), produto.getPreco(), produto.getTipo(), produto.getIngredientes());
        this.codigoM = codigo;
        this.qtd = qtd;        // setQtd(qtd);
        this.precoM = preco;
        this.situacao = situacao;
        this.horaEntrada = horaEntrada;
        this.horaTermino = horaTermino;
        this.ativo = ativo;
        this.removidos = removidos;
        this.adicionados = adicionados;
        this.tempo = tempo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProdutoMesaVO that = (ProdutoMesaVO) o;
        return Objects.equals(codigoM, that.codigoM) &&
                Objects.equals(qtd, that.qtd) &&
                Objects.equals(precoM, that.precoM) &&
                Objects.equals(ativo, that.ativo) &&
                Objects.equals(tempo, that.tempo) &&
                Objects.equals(situacao, that.situacao) &&
                Objects.equals(horaEntrada, that.horaEntrada) &&
                Objects.equals(horaTermino, that.horaTermino) &&
                Objects.equals(removidos, that.removidos) &&
                Objects.equals(adicionados, that.adicionados);
    }

    public int getTempo(){
        return this.tempo;
    }
    public void setTempo(int tempo){
        this.tempo = tempo;
    }
    public int getCodigoM() {
        return codigoM;
    }
    public void setCodigoM(int codigo) {
        this.codigoM = codigo;
    }
    public int getQtd() {
        return qtd;
    }
    public void setQtd (int qtd){
        this.precoM = super.getPreco()*qtd;

        if (this.adicionados != null && !this.adicionados.isEmpty()){
            for(IngredienteVO ingrediente : this.adicionados){
                this.precoM = this.precoM+ingrediente.getPreco();
            }
        }

        if (this.removidos != null && !this.removidos.isEmpty()){
            for(IngredienteVO ingrediente : this.removidos){
                this.precoM = this.precoM+ingrediente.getPreco();
            }
        }
        this.qtd = qtd;
    }
    public double getPrecoM() {
        return precoM;
    }
    public void setPrecoM(double preco) {
        this.precoM = preco;
    }
    public String getSituacao() {
        return situacao;
    }
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
    public String getHoraEntrada() {
        return horaEntrada;
    }
    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }
    public String getHoraTermino() {
        return horaTermino;
    }
    public void setHoraTermino(String horaTermino) {
        this.horaTermino = horaTermino;
    }
    public boolean isAtivo() {
        return ativo;
    }
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    public List<IngredienteVO> getRemovidos() {
        return removidos;
    }
    public void setRemovidos(List<IngredienteVO> removidos) {
        this.removidos = removidos;
    }
    public List<IngredienteVO> getAdicionados() {
        return adicionados;
    }
    public void setAdicionados(List<IngredienteVO> adicionados) {
        this.adicionados = adicionados;
    }

    public void setProduto(ProdutoVO produto){
        super.setCodigo(produto.getCodigo());
        super.setDescricao(produto.getDescricao());
        super.setIngredientes(produto.getIngredientes());
        super.setPreco(produto.getPreco());
        super.setTipo(produto.getTipo());
    }

    @Override
    public String toString() {
        return "ProdutoMesa [codigoM=" + codigoM + ", precoM=" + precoM + ", situacao=" + situacao + ", horaEntrada="
                + horaEntrada + ", horaTermino=" + horaTermino + ", ativo=" + ativo + ", removidos=" + removidos
                + ", adicionados=" + adicionados + "]";
    }

}
