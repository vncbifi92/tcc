package tcc_zanza.no_ip.biz.tcc_garcom.web_service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.text.LoginFilter;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.activity.CriaProdutoDaMesa;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.AdicionadoAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.IngredienteAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.dao.IngredienteDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;

/**
 * Created by VINICIUS on 10/19/2015.
 */
public class IngredienteWS extends AsyncTask<Void, Void, List<IngredienteVO>> {

    private String url;
    private int acao;
    private ProgressDialog progress;
    //private Activity activity;
    //private ListView listaAdicionados;
    //private ProdutoVO produto;

    public IngredienteWS(/*ProdutoVO produto, ListView listaAdicionados, Activity activity, */ String url, int acao){

        //this.activity = activity;
        this.acao = acao;
        this.url = url;
       // this.listaAdicionados = listaAdicionados;
        //this.produto = produto;
    }

    /*@Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(activity, "Aguarde..", "Carregando dados do servidor.");
    }*/

    @Override
    protected List<IngredienteVO> doInBackground(Void... params) {
        List<IngredienteVO> ingredientes = new ArrayList<IngredienteVO>();
        IngredienteDAO dao = new IngredienteDAO();
        switch (acao){
            case 1:
                try {
                    ingredientes = dao.buscaTodosIngredientes(url);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }

        return ingredientes;
    }

    @Override
    protected void onPostExecute(List<IngredienteVO> ingredientes) {
        //List<IngredienteVO> existentes = new ArrayList<>();

        if(ingredientes.size() > 0) {

            CriaProdutoDaMesa.addIngredientes(ingredientes);

            /*int tam = ingredientes.size();
            for(int i = 0 ; i<(tam-1) ; i++){
                if(!produto.getIngredientes().contains(ingredientes.get(i)) &&((
                        produto.getTipo().getDescricao().equals("Pastel - Especial")&&
                                ingredientes.get(i).getPreco() == 0.75) ||
                        (produto.getTipo().getDescricao().equals("Pastel")&&
                                ingredientes.get(i).getPreco() == 0.5))){
                    existentes.add(ingredientes.get(i));
                }

            }

            AdicionadoAdapter adicionarIngred = new AdicionadoAdapter(existentes, activity);

            listaAdicionados.setAdapter(adicionarIngred);*/
        }
        //progress.dismiss();
    }

}
