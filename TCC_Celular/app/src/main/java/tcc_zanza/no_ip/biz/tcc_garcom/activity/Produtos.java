package tcc_zanza.no_ip.biz.tcc_garcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.R;
import tcc_zanza.no_ip.biz.tcc_garcom.adapter.ProdutoAdapter;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ComandaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ComandaWS;
import tcc_zanza.no_ip.biz.tcc_garcom.web_service.ProdutoWS;

public class Produtos extends Activity {

    private String URLComanda;
    private String URLProduto;
    private String URLIngrediente;
    private String URLProdutoMesa;
    private String URLProdutoTipo;
    private String tipo;
    private static ComandaVO comanda;
    private static List<ProdutoVO> produtos = new ArrayList<>();


    public static void addComanda(ComandaVO comand){
        comanda = comand;
    }

    public static void addProdutos(List<ProdutoVO> produto){
        produtos = produto;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);

        ListView lista_produtos = (ListView)findViewById(R.id.lista);
        Button voltar = (Button)findViewById(R.id.activity_pedidos_da_mesa_botao_voltar);
        tipo = (String)getIntent().getSerializableExtra("tipo");
        URLProduto = (String)getIntent().getSerializableExtra("URLProduto");
        URLIngrediente = (String)getIntent().getSerializableExtra("URLIngrediente");
        URLProdutoMesa = (String)getIntent().getSerializableExtra("URLProdutoMesa");
        URLComanda = (String)getIntent().getSerializableExtra("URLComanda");
        URLProdutoTipo = (String)getIntent().getSerializableExtra("URLProdutoTipo");
        comanda = (ComandaVO) getIntent().getSerializableExtra("comanda");


        lista_produtos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProdutoVO produto = (ProdutoVO)parent.getItemAtPosition(position);

                Intent intent = new Intent(Produtos.this, CriaProdutoDaMesa.class);
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                intent.putExtra("comanda", comanda);
                intent.putExtra("produto", produto);

                startActivity(intent);

            }
        });

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Produtos.this, TipoProduto.class);
                intent.putExtra("URLProduto", URLProduto);
                intent.putExtra("URLComanda", URLComanda);
                intent.putExtra("URLIngrediente", URLIngrediente);
                intent.putExtra("URLProdutoMesa", URLProdutoMesa);
                intent.putExtra("URLProdutoTipo", URLProdutoTipo);
                intent.putExtra("comanda", comanda);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();
    }

    private void carregaLista(){

        ListView lista_pastel = (ListView)findViewById(R.id.lista);

        List<ProdutoVO> listaPastel = new ArrayList<>();

        for(ProdutoVO produto : produtos){
            if(produto.getTipo().getDescricao().equals(this.tipo)){
                listaPastel.add(produto);
            }
        }

        if(listaPastel.size() > 0) {
            ProdutoAdapter adapter = new ProdutoAdapter(listaPastel, this);

            lista_pastel.setAdapter(adapter);
        }



        /*ProdutoWS ws = new ProdutoWS(this, URLProduto, 1);
        ws.setTipo(tipo);
        ws.execute();*/
    }


}
