package tcc_zanza.no_ip.biz.tcc_garcom.dao;

import android.preference.PreferenceActivity;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tcc_zanza.no_ip.biz.tcc_garcom.vo.IngredienteVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoMesaVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoTipoVO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoVO;

/**
 * Created by VINICIUS on 10/22/2015.
 */
public class ProdutoMesaDAO {

    private ProdutoMesaVO produtoFromJson(JSONObject jPedidos) throws JSONException {

        ProdutoTipoVO tipo;
        List<IngredienteVO> adicionados = new ArrayList<>();
        List<IngredienteVO> removidos = new ArrayList<>();
        ArrayList<IngredienteVO> IngredienteVOs = new ArrayList<>();
        Gson gson = new Gson();

        JSONObject jTipo = jPedidos.getJSONObject("tipo");

        tipo = gson.fromJson(jTipo.toString(), ProdutoTipoVO.class);

        if(jPedidos.has("adicionados")) {
            try {
                JSONArray jAdicionados = jPedidos.getJSONArray("adicionados");
                IngredienteVO adicionado = new IngredienteVO();
                for(int i = 0 ; i<jAdicionados.length() ; i++){
                    JSONObject jAdicionado = jAdicionados.getJSONObject(i);

                    adicionado = gson.fromJson(jAdicionado.toString(), IngredienteVO.class);

                    adicionados.add(adicionado);
                }

            } catch (JSONException e2) {
                IngredienteVO adicionado = new IngredienteVO();
                JSONObject jAdicionados = jPedidos.getJSONObject("adicionados");
                adicionado = gson.fromJson(jAdicionados.toString(), IngredienteVO.class);

                adicionados.add(adicionado);
            }
        }

        if(jPedidos.has("removidos")) {
            try {
                JSONArray jRemovidos = jPedidos.getJSONArray("removidos");
                IngredienteVO removido = new IngredienteVO();
                for(int i = 0 ; i<jRemovidos.length() ; i++){
                    JSONObject jRemovido = jRemovidos.getJSONObject(i);

                    removido = gson.fromJson(jRemovido.toString(), IngredienteVO.class);

                    removidos.add(removido);
                }

            } catch (JSONException e2) {
                IngredienteVO removido = new IngredienteVO();
                JSONObject jRemovidos = jPedidos.getJSONObject("removidos");
                removido = gson.fromJson(jRemovidos.toString(), IngredienteVO.class);

                removidos.add(removido);
            }
        }

        try {
            JSONArray jIngredienteVOs = jPedidos.getJSONArray("ingredientes");
            IngredienteVO IngredienteVO = new IngredienteVO();
            for(int i = 0 ; i<jIngredienteVOs.length() ; i++){
                JSONObject jIngredient = jIngredienteVOs.getJSONObject(i);

                IngredienteVO = gson.fromJson(jIngredient.toString(), IngredienteVO.class);

                IngredienteVOs.add(IngredienteVO);
            }

        } catch (JSONException e2) {
            JSONObject jIngredient = jPedidos.getJSONObject("ingredientes");
            IngredienteVO IngredienteVO = new IngredienteVO();
            IngredienteVO = gson.fromJson(jIngredient.toString(), IngredienteVO.class);

            IngredienteVOs.add(IngredienteVO);
        }

        ProdutoMesaVO pedido = new ProdutoMesaVO(
                jPedidos.getInt("codigoM")
                , jPedidos.getInt("qtd")
                , new Double(jPedidos.getDouble("precoM")).doubleValue()
                , jPedidos.getString("situacao")
                , jPedidos.getString("horaEntrada")
                , null
                , jPedidos.getBoolean("ativo")
                , removidos
                , adicionados
                , jPedidos.getInt("tempo")
                , new ProdutoVO(
                jPedidos.getInt("codigo")
                , jPedidos.getString("descricao")
                , new Double(jPedidos.getDouble("preco")).doubleValue()
                , tipo
                , IngredienteVOs
        )
        );

        return pedido;

    }

    private ProdutoMesaVO buscaProdutoMesa(String url) throws IOException, JSONException {

        ProdutoMesaVO pedido;

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet getComanda = new HttpGet(url);

        HttpResponse response = httpClient.execute(getComanda);

        HttpEntity entity = response.getEntity();

        JSONObject jPedido = new JSONObject(EntityUtils.toString(entity)).getJSONObject("produtoMesa");

        pedido = produtoFromJson(jPedido);

        return pedido;
    }

    private JSONObject jsonFromProdutoMesa(ProdutoMesaVO produtoMesa) throws JSONException {

        JSONObject jTipo = new JSONObject();
        jTipo.put("codigo", produtoMesa.getTipo().getCodigo());
        jTipo.put("descricao", produtoMesa.getTipo().getDescricao());
        jTipo.put("tempo", produtoMesa.getTipo().getTempo());

        JSONObject jProdutoMesa = new JSONObject();
        jProdutoMesa.put("codigo", produtoMesa.getCodigo());
        jProdutoMesa.put("descricao", produtoMesa.getDescricao());
        jProdutoMesa.put("preco", produtoMesa.getPreco());
        jProdutoMesa.put("tipo", jTipo);

        if(produtoMesa.getIngredientes() != null && !produtoMesa.getIngredientes().isEmpty()){
            if(produtoMesa.getIngredientes().size() > 1) {
                JSONArray jIngredienteProd = new JSONArray();
                for (IngredienteVO ingrediente : produtoMesa.getIngredientes()) {
                    JSONObject jIngrediente = new JSONObject();
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                    jIngredienteProd.put(jIngrediente);
                }
                jProdutoMesa.put("ingredientes", jIngredienteProd);
            }else{
                JSONObject jIngrediente = new JSONObject();
                for (IngredienteVO ingrediente : produtoMesa.getIngredientes()) {
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                }
                jProdutoMesa.put("ingredientes", jIngrediente);
            }
        }

        jProdutoMesa.put("codigoM", produtoMesa.getCodigoM());
        jProdutoMesa.put("qtd", produtoMesa.getQtd());
        jProdutoMesa.put("precoM", produtoMesa.getPrecoM());
        jProdutoMesa.put("situacao", produtoMesa.getSituacao());

        jProdutoMesa.put("ativo", produtoMesa.isAtivo());

        if(produtoMesa.getRemovidos() != null && !produtoMesa.getRemovidos().isEmpty()) {
            if (produtoMesa.getRemovidos().size() > 1) {
                JSONArray jRemovidos = new JSONArray();
                for (IngredienteVO ingrediente : produtoMesa.getRemovidos()) {
                    JSONObject jIngrediente = new JSONObject();
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                    jRemovidos.put(jIngrediente);
                }
                jProdutoMesa.put("removidos", jRemovidos);
            } else {
                JSONObject jIngrediente = new JSONObject();
                for (IngredienteVO ingrediente : produtoMesa.getRemovidos()) {
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                }
                jProdutoMesa.put("removidos", jIngrediente);
            }
        }

        if(produtoMesa.getAdicionados() != null && !produtoMesa.getAdicionados().isEmpty()) {
            if (produtoMesa.getAdicionados().size() > 1) {
                JSONArray jAdicionados = new JSONArray();
                for (IngredienteVO ingrediente : produtoMesa.getAdicionados()) {
                    JSONObject jIngrediente = new JSONObject();
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                    jAdicionados.put(jIngrediente);
                }
                jProdutoMesa.put("adicionados", jAdicionados);
            } else {
                JSONObject jIngrediente = new JSONObject();
                for (IngredienteVO ingrediente : produtoMesa.getAdicionados()) {
                    jIngrediente.put("codigo", ingrediente.getCodigo());
                    jIngrediente.put("descricao", ingrediente.getDescricao());
                    jIngrediente.put("unidade", ingrediente.getUnidade());
                    jIngrediente.put("preco", ingrediente.getPreco());
                }
                jProdutoMesa.put("adicionados", jIngrediente);
            }
        }

        JSONObject jProdutoMesaF = new JSONObject();
        jProdutoMesaF.put("produtoMesa", jProdutoMesa);

        return jProdutoMesaF;

    }

    private static int pegaID(String url){

        int posicao = -1;
        String id = "";

        for(int i = (url.length()-1) ; i>=0 ; i--){
            if(url.charAt(i) == '/'){
                posicao = i;
                break;
            }
        }

        if(posicao > 0){
            for(int i = (posicao+1) ; i<url.length() ; i++){
                id = id+url.charAt(i);
            }
        }
        return Integer.parseInt(id);

    }

    public ProdutoMesaVO criaProdutosMesa (String url, ProdutoMesaVO produtosMesa) throws JSONException, IOException {

        HttpClient httpClient = new DefaultHttpClient();

        HttpPost postProdutoMesa = new HttpPost(url);

        JSONObject jProduto = jsonFromProdutoMesa(produtosMesa);

        StringEntity se = new StringEntity(jProduto.toString());
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        postProdutoMesa.setEntity(se);

        HttpResponse response = httpClient.execute(postProdutoMesa);
        String location = new String();
        for(Header header : response.getHeaders("Location")) {
            location = header.getValue();
        }
        return buscaProdutoMesa(url + pegaID(location));

    }

    public int getTempo (String url) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getproduto = new HttpGet(url+"tempo");

        HttpResponse response = httpClient.execute(getproduto);

        HttpEntity entity = response.getEntity();

        int tempo = Integer.parseInt(EntityUtils.toString(entity));

        return tempo;

    }
}
