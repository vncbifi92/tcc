package tcc_zanza.no_ip.biz.tcc_garcom.web_service;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import tcc_zanza.no_ip.biz.tcc_garcom.dao.ProdutoTipoDAO;
import tcc_zanza.no_ip.biz.tcc_garcom.vo.ProdutoTipoVO;

/**
 * Created by VINICIUS on 10/25/2015.
 */
public class ProdutoTipoWS extends AsyncTask<Void, Void, Void> {

    private int tempoPastel;
    private int tempoPastelEspecial;
    private String url;
    //private String tipo;

    public int getTempoPastelEspecial(){
        return this.tempoPastelEspecial;
    }
    public int getTempoPastel(){
        return this.tempoPastel;
    }

    /*public void setTipo(String tipo){
        this.tipo = tipo;
    }*/

    private void setTempoPastelEspecial(int tempoPastelEspecial){
        this.tempoPastelEspecial = tempoPastelEspecial;
    }

    private void setTempoPastel(int tempoPastel){
        this.tempoPastel = tempoPastel;
    }


    public ProdutoTipoWS(String url){
        this.url = url;
    }

    @Override
    protected Void doInBackground(Void... params) {
        ProdutoTipoDAO dao = new ProdutoTipoDAO();

        try {
            this.setTempoPastel(dao.getTempo(url, "Pastel"));
            this.setTempoPastelEspecial(dao.getTempo(url, "Pastel - Especial".replace(" ", "%20")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
